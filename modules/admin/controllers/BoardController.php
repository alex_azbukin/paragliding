<?php

namespace app\modules\admin\controllers;

use app\models\UserSearch;
use app\modules\admin\models\ClubSearch;
use app\modules\admin\models\EquipmentSearch;
use app\modules\admin\models\ProductSearch;
use app\modules\admin\models\SuspensionSearch;
use yii\web\Controller;
use Yii;

class BoardController extends Controller
{

    public function actionUsers(){
        $userSearch = new UserSearch();
        $dataProvider = $userSearch->search(Yii::$app->request->get());

        return $this->render('users',[
            'provider' => $dataProvider,
            'searchModel' => $userSearch,
        ]);
    }

    public function actionClubs(){
        $clubSearch = new ClubSearch();
        $dataProvider = $clubSearch->search(Yii::$app->request->get());

        return $this->render('clubs',[
            'provider' => $dataProvider,
            'searchModel' => $clubSearch,
        ]);
    }

    public function actionProducts(){
        $productSearch = new ProductSearch();
        $dataProvider = $productSearch->search(Yii::$app->request->get());

        return $this->render('products',[
            'provider' => $dataProvider,
            'searchModel' => $productSearch,
        ]);
    }

    public function actionSuspensions(){
        $suspensionSearch = new SuspensionSearch();
        $dataProvider = $suspensionSearch->search(Yii::$app->request->get());

        return $this->render('suspensions', [
            'provider' => $dataProvider,
            'searchModel' => $suspensionSearch,
        ]);
    }

    public function actionEquipments(){
        $equipmentSearch = new EquipmentSearch();
        $dataProvider = $equipmentSearch->search(Yii::$app->request->get());

        return $this->render('equipments', [
            'provider' => $dataProvider,
            'searchModel' => $equipmentSearch,
        ]);
    }

    public function actionIndex(){
        return $this->render('index', []);
    }

}