<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{

    public function actionIndex(){
        return $this->render('index', []);
    }

    public function actionAdmin(){
        return $this->render('default', []);
    }
}