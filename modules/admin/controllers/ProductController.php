<?php

namespace app\modules\admin\controllers;


use app\models\Product;
use app\modules\admin\models\ProductForm;
use yii\filters\AccessControl;
use app\modules\admin\models\Region;
use yii\web\Controller;
use Yii;
use yii\web;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\imagine\Image;

use yii\data\Pagination;

class ProductController extends Controller{

    public function actionIndex(){
        return $this->render('index', []);
    }

    public function actionCreateProduct(){
        $form = new ProductForm();
        $region = new Region();
        $destinationDropdown = $region->destinationDropdown();

        if($form->load(Yii::$app->request->post()) and $form->validate()) {
            $model = new Product();
            $form->image = UploadedFile::getInstance($form, 'image');
            $form->image_cropping = Yii::$app->request->post('image-cropping');
            if ($model->createProduct($form)) {
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/products']);
            }
            return $this->render('create-product', [
                'model' => $form,
                'destinationDropdown' => $destinationDropdown
            ]);
        }

            return $this->render('create-product',[
                'model' => $form,
                'destinationDropdown' => $destinationDropdown
            ]);
    }
//-------------------------------------------------------------------------------------------
    public function actionEditProduct($id)
    {
        $form = new ProductForm();
        $region = new Region();
        $dropdownRegions = $region->destinationDropdown();
        if (Yii::$app->request->isPost and ($form->load(Yii::$app->request->post())) and $form->validate()) {
            $product = Product::findOne($id);
            $form->image = UploadedFile::getInstance($form, 'image');
            $form->image_cropping = Yii::$app->request->post('image-cropping');
            if ($product->updateData($form)) {
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/products']);
            }

            Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
            Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
            return $this->redirect(['/admin/board/products']);
        }
        else{
            $product = Product::findOne($id);
            $form->image_small_path = $product->image_small_path;
            $form->title = $product->title;
            $form->description = $product->description;
            $form->type_id = $product->type_id;
            $form->region_id = $product->region_id;
            $form->owner_id = $product->owner_id;
            $form->id = $product->id;

            return $this->render('edit-product', [
                'model' => $form,
                'dropdownRegions' => $dropdownRegions,
                'id' => $id
            ]);
        }
    }
//---------------------------------------------------------------------------
    public function actionDeleteProduct($id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $product = Product::findOne($id);
            if($product->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Product was deleted successful'),
                    'type' => 'products'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Error. Product wasn`t deleted'),
                    'type' => 'products'
                ];
            }
            echo Json::encode($response);
        }
    }
}