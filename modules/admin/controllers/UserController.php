<?php

namespace app\modules\admin\controllers;

use app\components\EventHandler;
use app\components\MailerEvent;
use app\models\UserEditForm;
use app\models\Parapro;
use app\modules\admin\models\Region;
use app\models\User;
use yii\web\Controller;
use Yii;
use yii\web;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\imagine\Image;

use yii\data\Pagination;

class UserController extends Controller{

    public function actionCheckClaim($id){
        if(isset($id)){
            $user = User::findOne(['id' => $id]);
            if($user->claimPilotStatus() == 0) {
                return $this->render('check-claim', [
                    'user' => $user
                ]);
            }
            else if($user->claimPilotStatus() == 1){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-danger');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Claim already is approved'));
                return $this->redirect(['/admin/board/users']);
            }
            else if($user->claimPilotStatus() == 2){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-danger');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Claim already is rejected'));
                return $this->redirect(['/admin/board/users']);
            }
            else if($user->claimPilotStatus() < 0){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-danger');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Claim does not exist'));
                return $this->redirect(['/admin/board/users']);
            }
        }
        return $this->goHome();
    }
//----------------------------------------------------------------------------------------------------------------------

    public function actionEdit($id){
        $model = new UserEditForm();
        $parapro = new Parapro();
        $region = new Region();
        $dropdownRegions = $region->destinationDropdown();
        $levelsList = $parapro->levelsList();
        if(Yii::$app->request->isPost and ($model->load(Yii::$app->request->post())) && $model->validate()) {
                $user = User::findOne(['id' => $model->id]);
                $model->image = UploadedFile::getInstance($model, 'image');
                $model->image_cropping = Yii::$app->request->post('image-cropping');
                if($user->updateUser($model)){
                    Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                    Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                    return $this->redirect(['/admin/board/users']);
                }
            return $this->render('edit', [
                'model' => $model,
                'dropdownRegions' => $dropdownRegions,
                'levelsList' => $levelsList
            ]);


        }
        else{
            $user = User::findOne(['id' => $id]);
            $model->image_small_path = $user->image_small_path;
            $model->id = $user->id;
            $model->email = $user->email;
            $model->username = $user->username;
            $model->first_name = $user->first_name;
            $model->second_name = $user->second_name;
            $model->last_name = $user->last_name;
            $model->birthday = $user->birthday;
            $model->region_id = $user->region_id;
            $model->city = $user->city;
            $model->phone = $user->phone;
            $model->description = $user->description;
            $model->participant_visits_checkbox = $user->participant_visits_checkbox;
            $model->participant_competition_checkbox = $user->participant_competition_checkbox;
            $model->ukraine_license_number = $user->ukraine_license_number;
            $model->parapro_id = $user->parapro_id;
            $model->fai_license_number = $user->fai_license_number;
            $model->license_checkbox = $user->license_checkbox;
            $model->insurance_checkbox = $user->insurance_checkbox;
            $model->fly_since = $user->fly_since;
            $model->fly_hours_amount = $user->fly_hours_amount;
            $model->average_fly_hours_amount = $user->average_fly_hours_amount;
            $model->wing = $user->wing;
            $model->reserve_parachute = $user->reserve_parachute;
            $model->helmet = $user->helmet;
            $model->devices = $user->devices;
            return $this->render('edit', [
                'model' => $model,
                'dropdownRegions' => $dropdownRegions,
                'levelsList' => $levelsList
            ]);
            return $this->goHome();
        }
    }

    public function actionUploadUserImage(){
        $photo =Yii::$app->request->post();
        $path_to_tmp_dir = Yii::$app->basePath.'/web/images/users/tmp/';
        $image = UploadedFile::getInstanceByName('file');
        $path_to_original_image = $path_to_tmp_dir . $image->baseName . '.' . $image->extension;
        $image->saveAs($path_to_original_image);
        $img = Image::crop($path_to_original_image, $photo['w'], $photo['h'], [$photo['x'], $photo['y']])->save($path_to_original_image);

        var_dump($img);
        var_dump($image);
        var_dump($photo);
    }



//----------------------------------------------------------------------------------------------------------------------
    public function actionRejectClaimPilot($id){
        if(isset($id)){
            $user = User::findOne(['id' => $id]);
            $user->claimPilot->status = 2;
            $user->claimPilot->save();
            $eventHandler = new EventHandler();
            $mailerEvent = new MailerEvent();
            $mailerEvent->user = $user;
            $eventHandler->on(EventHandler::EVENT_REJECT_CLAIM_PILOT, [$mailerEvent, 'rejectClaimPilot']);
            $eventHandler->rejectClaimPilot();
            Yii::$app->session->setFlash('adminBoardPageClass', 'alert-danger');
            Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Claim was rejected'));
            return $this->redirect(['/admin/board/users']);

        }
        return $this->goHome();
    }

    public function actionApproveClaimPilot($id){
        if(isset($id)){
            $user = User::findOne(['id' => $id]);
            $user->role_id = 2;
            $user->claimPilot->status = 1;
            $user->claimPilot->save();
            if($user->save()) {
                $eventHandler = new EventHandler();
                $mailerEvent = new MailerEvent();
                $mailerEvent->user = $user;
                $eventHandler->on(EventHandler::EVENT_APPROVE_CLAIM_PILOT, [$mailerEvent, 'approveClaimPilot']);
                $eventHandler->approveClaimPilot();
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Claim was approved successful'));
                return $this->redirect(['/admin/board/users']);
            }

        }
        return $this->goHome();
    }

    public function actionDelete($id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $user = User::findOne($id);
            if($user->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'User was deleted successful'),
                    'type' => 'users'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Error. User wasn`t deleted'),
                    'type' => 'users'
                ];
            }
            echo Json::encode($response);
        }
    }

}
