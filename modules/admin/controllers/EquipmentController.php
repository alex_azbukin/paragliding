<?php
namespace app\modules\admin\controllers;

use app\models\Equipment;
use yii\web\Controller;
use app\modules\admin\models\EquipmentForm;
use Yii;
use yii\web;
use yii\helpers\Json;

class EquipmentController extends Controller{

    public function actionIndex(){
        return $this->render('index', []);
    }

    public function actionCreateEquipment(){
        $form = new EquipmentForm();
        if($form->load(Yii::$app->request->post()) and $form->validate()){
            $equipment = new Equipment();
            $equipment->name = $form->name;
            if($equipment->save($form)){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/equipments/']);
            }
            return $this->render('create-equipment', [
                'model' => $form
            ]);
        }
        return $this->render('create-equipment', [
            'model' => $form
        ]);
    }

    public function actionEditEquipment($id){

        $form = new EquipmentForm();
        if(Yii::$app->request->isPost and ($form->load(Yii::$app->request->post())) and $form->validate()){
            $model = Equipment::findOne($id);
            if($model->updateData($form)){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/equipments']);
            }

            return $this->render('edit-equipment', [
                'model' => $form,
                'id' =>$id
            ]);
        }
        else{
            $model = Equipment::findOne($id);
            $form->name = $model->name;
            $form->id = $model->id;
            return $this->render('edit-equipment', [
                'model' => $form,
                'id' => $id
            ]);
        }
    }

    public function actionDeleteEquipment($id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $equipment = Equipment::findOne($id);
            if($equipment->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Equipment was deleted successful'),
                    'type' => 'equipments'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Error. Equipment wasn`t deleted'),
                    'type' => 'equipments'
                ];
            }
            echo Json::encode($response);
        }
    }
}
