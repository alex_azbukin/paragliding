<?php
namespace app\modules\admin\controllers;

use app\models\Suspensions;
use yii\web\Controller;
use app\modules\admin\models\SuspensionForm;
use Yii;
use yii\web;
use yii\helpers\Json;

class SuspensionController extends Controller{

    public function actionIndex(){
        return $this->render('index', []);
    }


    public function actionCreateSuspension(){

        $form = new SuspensionForm();
        if($form->load(Yii::$app->request->post()) and $form->validate()){
            $suspension = new Suspensions();
            $suspension->name = $form->name;
            if($suspension->save($form)){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/suspensions/']);
            }
            return $this->render('create-suspension', [
                'model' => $form
            ]);
        }
        return $this->render('create-suspension', [
            'model' => $form
        ]);
    }

    public function actionEditSuspension($id){

        $form = new SuspensionForm();
        if(Yii::$app->request->isPost and ($form->load(Yii::$app->request->post())) and $form->validate()){
            $model = Suspensions::findOne($id);
            if($model->updateData($form)){
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/suspensions']);
            }

            return $this->render('edit-suspension', [
                'model' => $form,
                'id' =>$id
            ]);
        }
        else{
            $model = Suspensions::findOne($id);
            $form->name = $model->name;
            $form->id = $model->id;
            return $this->render('edit-suspension', [
                'model' => $form,
                'id' => $id
            ]);
        }
    }

    public function actionDeleteSuspension($id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $suspension = Suspensions::findOne($id);
            if($suspension->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Suspension was deleted successful'),
                    'type' => 'suspensions'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Error. Suspension wasn`t deleted'),
                    'type' => 'suspensions'
                ];
            }
            echo Json::encode($response);
        }
    }


}
