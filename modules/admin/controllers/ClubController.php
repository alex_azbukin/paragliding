<?php

namespace app\modules\admin\controllers;

use app\models\User;
use app\modules\admin\models\Club;
use app\modules\admin\models\ClubForm;
use app\modules\admin\models\Region;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\imagine\Image;

use yii\data\Pagination;

class ClubController extends Controller{

    public function actionIndex(){
        return $this->render('index', []);
    }

    public function actionCreateClub(){

        $clubForm = new ClubForm();
        $region = new Region();
        $destinationDropdown = $region->destinationDropdown();

        if($clubForm->load(Yii::$app->request->post()) and $clubForm->validate()){
            $clubModel = new Club();
            $clubForm->image = UploadedFile::getInstance($clubForm, 'image');
            $clubForm->image_cropping = Yii::$app->request->post('image-cropping');
            if($clubModel->createClub($clubForm)) {
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/clubs']);
            }
            return $this->render('create-club',[
                'model' => $clubForm,
                'destinationDropdown' => $destinationDropdown
            ]);
        }

        return $this->render('create-club',[
            'model' => $clubForm,
            'destinationDropdown' => $destinationDropdown
        ]);

    }

    public function actionEditClub($club_id)
    {
            $clubForm = new ClubForm();
            $region = new Region();
            $dropdownRegions = $region->destinationDropdown();
            if (Yii::$app->request->isPost and ($clubForm->load(Yii::$app->request->post())) and $clubForm->validate()) {
                    $club = Club::findOne($club_id);
                    $clubForm->image = UploadedFile::getInstance($clubForm, 'image');
                    $clubForm->image_cropping = Yii::$app->request->post('image-cropping');
                    if ($club->updateData($clubForm)) {
                        Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                        Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                        return $this->redirect(['/admin/board/clubs']);
                    }
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/admin/board/clubs']);
            }
            else{
                $club = Club::findOne($club_id);
                $clubForm->image_small_path = $club->image_small_path;
                $clubForm->name = $club->name;
                $clubForm->admin_name = $club->admin[0]->username;
                $clubForm->admin_id = $club->admin[0]->id;
                $clubForm->region_id = $club->region_id;
                $clubForm->latitude = $club->latitude;
                $clubForm->longitude = $club->longitude;
                $clubForm->club_id = $club->id;

                return $this->render('edit-club', [
                    'model' => $clubForm,
                    'dropdownRegions' => $dropdownRegions,
                ]);
            }
    }

//----------------------------------------------------------------
    public function actionDeleteClub($club_id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $club = Club::findOne($club_id);
            if($club->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $club_id,
                    'msg' => Yii::t('app', 'Club was deleted successful'),
                    'type' => 'clubs'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $club_id,
                    'msg' => Yii::t('app', 'Error. Club wasn`t deleted'),
                    'type' => 'clubs'
                ];
            }
            echo Json::encode($response);
        }
    }


    public function actionSearchAdmin(){
        $user_name_or_email = \Yii::$app->request->getQueryParam('user_name_or_email', null);
        if(!is_null($user_name_or_email)) {
            $users = User::find()->where(['role_id' => 2])
                ->andFilterWhere(['or',
                    ['like', 'username', $user_name_or_email],
                    ['like', 'email', $user_name_or_email]])
                ->all();
            $admins = [];
            foreach ($users as $user) {
                $admins[$user->id] = $user->username.'('.$user->email.')';
            }
            if (count($admins)) {
                $response = [
                    'status' => 'ok',
                    'users' => $admins
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'message' => Yii::t('app', 'Pilot with this username or email doesn`t exist')
                ];
            }
        }
        else{
            $response = [
                'status' => 'error'
            ];
        }
        echo Json::encode($response);
    }

}
