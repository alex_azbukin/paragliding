<?php
$this->title = Yii::t('app', 'Product creating');
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use cyneek\yii2\widget\upload\crop\UploadCrop;
?>


<?php $form = ActiveForm::begin([
    'id' => 'create-product',
    'action' => Url::toRoute(['/admin/product/create-product']),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]);?>

    <div class="col-xs-12 form-title">
        <h1><?= Yii::t('app', 'Product creating');?></h1>
    </div>

    <div class="col-xs-12">
            <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
                'jcropOptions' => [
                    'aspectRatio' => 1,
                    'rotatable' => false,
                    'minContainerWidth' => 200,
                    'minContainerHeight' => 200
                ]
            ]); ?>
    </div>


        <?= $form->field($model, 'title')->textInput();?>
        <?= $form->field($model, 'description')->textarea();?>
        <?= $form->field($model, 'region_id')->dropDownList($destinationDropdown, ['prompt' => Yii::t('app','Choose region')]);?>


    <?= $form->field($model, 'owner_id')->hiddenInput()->label('');?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label('');?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success col-xs-4 save-product', 'id' => 'create-product']);?>
        <?=Html::a('Назад', ['/admin/board/products'], ['class' => 'btn btn-default col-xs-4 pull-right back-product']);?>
        </div>
    </div>



<?php $form = ActiveForm::end() ?>
