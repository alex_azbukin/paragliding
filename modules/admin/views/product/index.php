<?php
use yii\helpers\Html;
?>
<?=Html::a('Добавить товар', ['/admin/product/#'], ['class' => 'btn btn-success col-xs-4 add-product', 'id' => 'add-product']);?>

<?=Html::a('Назад', ['/admin/product/index'], ['class' => 'btn btn-default col-xs-4 pull-right back-product']);?>