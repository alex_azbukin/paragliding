<?php
$this->title = Yii::t('app', 'Editing product');
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use cyneek\yii2\widget\upload\crop\UploadCrop;
?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-product',
    'action' => Url::toRoute(['/admin/product/edit-product', 'id' => $model->id]),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]); ?>


    <div class="col-xs-12 form-title">
        <h1><?= Yii::t('app', 'Editing product');?></h1>
    </div>
    <div class="col-xs-12">
        <div class="form-group field-clubform-name has-success">
            <label class="col-sm-3 control-label" for="clubform-name"><?=Yii::t('app', 'Current product image');?></label>
            <div class="col-sm-6">
                <img src="<?=$model->image_small_path;?>" class="img-responsive">
            </div>
        </div>
        <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
            'jcropOptions' => [
                'aspectRatio' => 1,
                'rotatable' => false,
                'minContainerWidth' => 200,
                'minContainerHeight' => 200
            ]
        ]); ?>
    </div>
<?= $form->field($model, 'title')->textInput();?>
<?= $form->field($model, 'description')->textarea();?>
<?= $form->field($model, 'region_id')->dropDownList($dropdownRegions,  ['prompt' => Yii::t('app','Choose region')]);?>

<?= $form->field($model, 'owner_id')->hiddenInput()->label('');?>
<?= $form->field($model, 'type_id')->hiddenInput()->label('');?>
    <br>




    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success col-sm-6', 'name' => 'edit-product-button']);?>

            <?= Html::a(Yii::t('app', 'Back'), ['/admin/board/products'], ['class' => 'btn btn-primary col-xs-4 pull-right']) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>