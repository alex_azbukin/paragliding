<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;
use yii\bootstrap\Dropdown;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="admin">
<?php $this->beginBody() ?>

<header>
    <section class="header admin">
        <?php
        NavBar::begin([
            'brandLabel' => 'Paraplan',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
                'id' => 'custom-header'
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => [
                ['label' => Yii::t('app', 'Federation'), 'url' => ['/']],
                ['label' => Yii::t('app', 'Clubs'), 'url' => ['/club/index']],
                ['label' => Yii::t('app', 'Contests'), 'url' => ['/']],
                ['label' => Yii::t('app', 'Schools'), 'url' => ['/']],
                ['label' => Yii::t('app', 'Fly`s points'), 'url' => ['/']],
                ['label' => Yii::t('app', 'Forum'), 'url' => ['/']],
                ['label' => Yii::t('app', 'Callboard'), 'url' => ['/product/index']]
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Calendar', 'url' => ['/']],
                Yii::$app->user->isGuest ?
                    ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']] :
                    ['label' =>   Yii::t('app', 'Welcome').' , '.Yii::$app->user->identity->username,
                        'items' => [
                            ['label' => Yii::t('app','Profile'), 'url' => ['/user/profile']],
                            (Yii::$app->user->identity->role_id == 4)?
                                ['label' => Yii::t('app','Admin panel'), 'url' => ['/admin/board/users']]:'',

                            ['label' => Yii::t('app','Logout'),
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                            ],
                        ]
                    ]
            ],
        ]);
        NavBar::end();
        ?>
    </section>
</header>

<div class="wrap">
    <div class="container">
        <div class="row admin">
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <span class="copyright">&copy; <?= date('Y') ?> Paraplan </span>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
