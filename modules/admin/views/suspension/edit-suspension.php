<?php
$this->title = Yii::t('app', 'Editing suspension');
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-suspension',
    'action' => Url::toRoute(['/admin/suspension/edit-suspension', 'id' => $model->id]),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]);?>


    <div class="col-xs-12 form-title">
        <h1><?= Yii::t('app', 'Editing suspension');?></h1>
    </div>

<?= $form->field($model, 'name')->textInput();?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success col-sm-6', 'name' => 'edit-suspension-button']);?>

            <?= Html::a(Yii::t('app', 'Back'), ['/admin/board/suspensions'], ['class' => 'btn btn-primary col-xs-4 pull-right']) ?>
        </div>
    </div>


<?php $form = ActiveForm::end() ?>