<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Helper;
?>
<div class="admin-panel">
    <?= \app\components\AdminNavbarWidget::widget(['active_link' => 'users']);?>
    <?php if(Yii::$app->session->getFlash('adminBoardPageClass') and Yii::$app->session->getFlash('adminBoardPageMessage')):?>
        <?php echo
        \yii\bootstrap\Alert::widget([
                'options' =>
                    ['class' => Yii::$app->session->getFlash('adminBoardPageClass') ],
                'body' => Yii::$app->session->getFlash('adminBoardPageMessage')
            ]
        )
        ?>
    <?php endif;?>
    <div class="row">
        <div class="col-xs-12">
            <?php
            echo GridView::widget([
                'rowOptions' => function ($model, $key, $index, $grid){
                    return [
                        'data-element-id' => $model->id,
                        'class' => 'users',
                    ];
                },
                'dataProvider' => $provider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => Yii::t('app','Current user image'),
                        'format' => 'html',
                        'value' => function($model){
                            return Helper::getImage($model->image_small_path, 'user', 'img-responsive table-image');
                        }
                    ],
                    'email',
                    'username',
                    [
                        'attribute' => 'role_id',
                        'format' => 'html',
                        'value' => function($model){
                            if($model->role_id == 1){
                                return Html::tag('span', Yii::t('app', 'Willing'), ['class' => 'label label-default']);
                            }elseif($model->role_id == 2){
                                return Html::tag('span', Yii::t('app', 'Pilot'), ['class' => 'label label-success']);
                            }elseif($model->role_id == 3){
                                return Html::tag('span', Yii::t('app', ''), ['class' => '']);
                            }
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($model){
                            return date('Y-M-d', $model->updated_at);
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function($model){
                            return date('Y-M-d', $model->created_at);
                        }
                    ],

                    [
                        'attribute' => Yii::t('app', 'Actions'),
                        'format' => 'html',
                        'value' => function($model){
                            $actions = '';
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil edit']), \yii\helpers\Url::toRoute(['/admin/user/edit', 'id' => $model->id]), ['class' => 'actions col-xs-6']);
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']), \yii\helpers\Url::toRoute(['/admin/user/delete', 'id' => $model->id]), ['class' => 'actions delete col-xs-6']);
                            return $actions;
                        }
                    ],
                ],
            ]);
            ?>
            </div>
        </div>
</div>