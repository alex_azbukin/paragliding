<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Helper;
use app\models\User;
?>
<div class="admin-panel">
<?= \app\components\AdminNavbarWidget::widget(['active_link' => 'products']);?>
<?php if(Yii::$app->session->getFlash('adminBoardPageClass') and Yii::$app->session->getFlash('adminBoardPageMessage')):?>
    <?php echo
    \yii\bootstrap\Alert::widget([
            'options' =>
                ['class' => Yii::$app->session->getFlash('adminBoardPageClass') ],
            'body' => Yii::$app->session->getFlash('adminBoardPageMessage')
        ]
    )
    ?>
<?php endif;?>
    <div class="row">
        <div class="col-xs-12">
            <?=Html::a(Yii::t('app', 'Create product'), ['/admin/product/create-product'], ['class' => 'btn btn-success col-xs-3']);?>
        </div><br><br><br>
        <div class="col-xs-12">
            <?php

            echo GridView::widget([
                'class' => ['grid-view'],
                'rowOptions' => function ($model, $key, $index, $grid){
                    return [
                        'data-element-id' => $model->id,
                        'class' => 'products',
                    ];
                },
                'dataProvider' => $provider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => Yii::t('app','User'),
                        'format' => 'html',
                        'value' => function($model){
                            return Html::a(Helper::getImage($model->owner->image_small_path, 'user', 'img-responsive table-image'), ['/admin/user/edit', 'id' => $model->owner->id]);
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Advertisement name'),
                        'value' => function($model){
                            return $model->title;
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Advertisement description'),
                        'value' => function($model){
                            return $model->description;
                        }
                    ],

                    [
                        'attribute' => Yii::t('app','Image'),
                        'format' => 'html',
                        'value' => function($model){
                            return Helper::getImage($model->image_small_path, 'product', 'img-responsive table-image');
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Update'),
                        'value' => function($model){
                            return date('Y-M-d', $model->updated_at);
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Create'),
                        'value' => function($model){
                            return date('Y-M-d', $model->created_at);
                        }
                    ],

                    [
                        'attribute' => Yii::t('app', 'Actions'),
                        'format' => 'html',
//
                        'value' => function($model){
                            $actions = '';
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil ']), \yii\helpers\Url::toRoute(['/admin/product/edit-product', 'id' => $model->id]), ['class' => 'actions edit col-xs-6']);
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash ']), \yii\helpers\Url::toRoute(['/admin/product/delete-product', 'id' => $model->id]), ['class' => 'actions delete col-xs-6']);
                            return $actions;
                        }
                    ],
                ],
            ]);
            ?>
        </div>
    </div>


</div>

