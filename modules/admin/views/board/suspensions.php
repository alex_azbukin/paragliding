<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Helper;
?>
<div class="admin-panel">
    <?= \app\components\AdminNavbarWidget::widget(['active_link' => 'suspensions']);?>
    <?php if(Yii::$app->session->getFlash('adminBoardPageClass') and Yii::$app->session->getFlash('adminBoardPageMessage')):?>
        <?php echo
        \yii\bootstrap\Alert::widget([
                'options' =>
                    ['class' => Yii::$app->session->getFlash('adminBoardPageClass') ],
                'body' => Yii::$app->session->getFlash('adminBoardPageMessage')
            ]
        )
        ?>
    <?php endif;?>

    <div class="row">
        <div class="col-xs-12">
            <?=Html::a(Yii::t('app', 'Create suspension'), ['/admin/suspension/create-suspension'], ['class' => 'btn btn-success col-xs-3']);?>
        </div><br><br><br>
        <div class="col-xs-12">
            <?php
            echo GridView::widget([
                'class' => ['grid-view'],
                'rowOptions' => function ($model, $key, $index, $grid){
                    return [
                        'data-element-id' => $model->id,
                        'class' => 'suspensions',
                    ];
                },
                'dataProvider' => $provider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => Yii::t('app','Suspension name'),
                        'value' => function($model){
                            return $model->name;
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Update'),
                        'value' => function($model){
                            return date('Y-M-d', $model->updated_at);
                        }
                    ],
                    [
                        'attribute' => Yii::t('app','Create'),
                        'value' => function($model){
                            return date('Y-M-d', $model->created_at);
                        }
                    ],

                    [
                        'attribute' => Yii::t('app', 'Actions'),
                        'format' => 'html',
//
                        'value' => function($model){
                            $actions = '';
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil ']), \yii\helpers\Url::toRoute(['/admin/suspension/edit-suspension', 'id' => $model->id]), ['class' => 'actions edit col-xs-6']);
                            $actions .= Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash ']), \yii\helpers\Url::toRoute(['/admin/suspension/delete-suspension', 'id' => $model->id]), ['class' => 'actions delete col-xs-6']);
                            return $actions;
                        }
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>