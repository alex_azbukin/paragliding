<?php
use yii\helpers\Html;
?>
<div class="col-xs-12 user-edit">
    <div class="col-xs-12 page-title">
        <h1><?= Yii::t('app', 'Claim to be a Pilot');?></h1>
    </div>

    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Email');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->email;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Username');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->username;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Last name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->last_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'First name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->first_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Second name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->second_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Birthday');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->birthday;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'About myself');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->description;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Region');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=(isset($user->region->name)) ? $user->region->name : '';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'City');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->city;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Parapro level');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=(isset($user->parapro->name)) ? $user->parapro->name : '';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Fly since');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->fly_since;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Fly hours amount');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->fly_hours_amount;?></div>
    </div>

    <div class="col-xs-12">
        <?=Html::a(Yii::t('app', 'Approve'), ['/admin/user/approve-claim-pilot', 'id' => $user->id], ['class' => 'btn btn-success col-xs-3']);?>
        <?=Html::a(Yii::t('app', 'Reject'), ['/admin/user/reject-claim-pilot', 'id' => $user->id], ['class' => 'btn btn-warning col-xs-offset-1 col-xs-3']);?>
        <?=Html::a(Yii::t('app', 'Back'), ['/admin/board/users'], ['class' => 'btn btn-default col-xs-offset-1 col-xs-3']);?>
    </div>
</div>