<?php
use yii\helpers\Html;
use app\models\User;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use cyneek\yii2\widget\upload\crop\UploadCrop;
?>


<?php $form = ActiveForm::begin([
    'id' => 'edit',
    'action' => Url::to(['/admin/user/edit', 'id' => $model->id]),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],

    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]); ?>


<div class="col-xs-12 user-edit">
    <div class="col-xs-12 page-title">
        <h1><?= Yii::t('app', 'User profile');?></h1>
    </div>

    <div class="col-xs-12">
        <div class="form-group field-userform-name has-success">
            <label class="col-sm-3 control-label" for="userform-name"><?=Yii::t('app', 'Current user image');?></label>
            <div class="col-sm-6">
                <img src="<?=$model->image_small_path;?>" class="img-responsive">
            </div>
        </div>
        <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
            'jcropOptions' => [
                'aspectRatio' => 1,
                'rotatable' => false,
                'minContainerWidth' => 200,
                'minContainerHeight' => 200
            ]
        ]); ?>
    </div>


    <?= $form->field($model, 'email')->input('text', ['readonly' => 'true']) ?>
    <?= $form->field($model, 'username')->input('text', ['readonly' => 'true'])?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'first_name') ?>
    <?= $form->field($model, 'second_name') ?>
    <?= $form->field($model, 'phone',
        ['template' =>
            "{label}\n<div class=\"col-sm-6\">
            <div class=\"input-group\"><div class=\"input-group-addon\">+380</div>{input}</div>
            </div>\n<div class=\"col-xs-6\">{error}</div>"
        ])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '99-9-999-999',
    ]) ?>
    <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
        'options' => [
            'placeholder' => Yii::t('app', 'Select birthday'),
        ],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
            'todayHighlight' => true
        ]])?>
    <?= $form->field($model, 'region_id')->dropDownList($dropdownRegions,  ['prompt' => Yii::t('app','Choose region')]);?>
    <?= $form->field($model, 'city')?>
    <?= $form->field($model, 'description')->textarea(['rows' => '3', 'maxlength' => 250]);?>
    <?= $form->field($model, 'parapro_id')->dropDownList($levelsList,  ['prompt' => Yii::t('app','Choose level')]);?>
    <?= $form->field($model, 'fly_since')->widget(DatePicker::classname(), [
        'options' => [
            'placeholder' => Yii::t('app', 'Select date'),
        ],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
            'todayHighlight' => true
        ]])?>
    <?= $form->field($model, 'fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
    <?= $form->field($model, 'average_fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
    <?= $form->field($model, 'wing')?>
    <?= $form->field($model, 'reserve_parachute')?>
    <?= $form->field($model, 'helmet')?>
    <?= $form->field($model, 'devices')?>
    <?= $form->field($model, 'id')->hiddenInput()->label('');?>

    <?php ActiveForm::end(); ?>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success col-sm-6', 'name' => 'edit-profile-button']);?>

            <?= Html::a(Yii::t('app', 'Back'), ['/admin/board/users'], ['class' => 'btn btn-primary col-xs-4 pull-right']) ?>
        </div>
    </div>

</div>