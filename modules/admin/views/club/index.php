<?php
use yii\helpers\Html;
?>
<?=Html::a('Добавить клуб', ['/admin/club/#'], ['class' => 'btn btn-success col-xs-4 add-club', 'id' => 'add-club']);?>

<?=Html::a('Назад', ['/admin/club/index'], ['class' => 'btn btn-default col-xs-4 pull-right back-club']);?>