<?php
$this->title = Yii::t('app', 'Club creating');
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use cyneek\yii2\widget\upload\crop\UploadCrop;
?>

<?php $form = ActiveForm::begin([
    'id' => 'create-club-form',
    'action' => Url::toRoute(['/admin/club/create-club']),
    'options' => [
        'class' => 'form-vertical admin',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
//        'template' => "{label}\n<div class=\"col-xs-12 \">{input}</div>\n<div class=\"col-xs-12\">{error}</div>",
//        'labelOptions' => ['class' => 'col-xs-12 control-label label-get-tour'],
    ],
]);?>


<div class="row">
        <div class="col-xs-12 section-title"><h1><?= Yii::t('app', 'Club creating');?></h1></div>
    <div class="col-xs-12">
        <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
            'jcropOptions' => [
                'aspectRatio' => 1,
                'rotatable' => false,
                'minContainerWidth' => 200,
                'minContainerHeight' => 200
            ]
        ]); ?>
    </div>
    <div class="col-xs-3">


        <?= $form->field($model, 'admin_name')->textInput();?>

        <?= $form->field($model, 'admin_id')->hiddenInput()->label('');?>
    </div>

    <div class="col-xs-3">
        <?= $form->field($model, 'name')->textInput();?>
    </div>
    <div class="col-xs-3">
        <?= $form->field($model, 'region_id')->dropDownList($destinationDropdown,
           ['prompt' => Yii::t('app','Choose region')]);?>
    </div>

</div>
<div class="row">
    <div class="col-xs-3"><?= $form->field($model, 'latitude')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '99.99999']);?></div>
    <div class="col-xs-3"><?= $form->field($model, 'longitude')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '99.99999']);?></div>
</div>

<?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success col-xs-4 save-club', 'id' => 'create-club']);?>
<?=Html::a('Назад', ['/admin/board/clubs'], ['class' => 'btn btn-default col-xs-4 pull-right back-club']);?>
<?=Html::a('', ['/admin/club/search-admin'], ['class' => 'hidden search-admin']);?>

<?php $form = ActiveForm::end() ?>

