<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\web;

class ClubForm extends Model{

    public $admin_id;
    public $admin_name;
    public $name;
    public $region_id;
    public $image;
    public $latitude;
    public $longitude;
    public $image_small_path;
    public $club_id;
    public $image_cropping;

    public function rules(){
        return [
            [['admin_name', 'admin_id', 'name', 'region_id', 'latitude', 'longitude'], 'required'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif']],
            [['club_id', 'image'], 'default', 'value' => 0]
        ];
    }

    public function attributeLabels(){
        return [
            'admin_name' => Yii::t('app', 'Admin name'),
            'name' => Yii::t('app', 'Club name'),
            'region_id' => Yii::t('app', 'Region'),
            'image' => Yii::t('app', 'Club Image'),
            'crop_info' => Yii::t('app', 'Club Image'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'image_small_path' => Yii::t('app', 'Current club image'),
        ];
    }


}