<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\web;

class EquipmentForm extends Model{

    public $name;
    public $id;

    public function rules(){
        return [
          ['name','required']
        ];
    }

    public function attributeLabels(){
        return [
          'name' => Yii::t('app', 'Equipment name')
        ];
    }


}