<?php
namespace app\modules\admin\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class ClubAdmins extends ActiveRecord{

    public static function tableName()
    {
        return 'clubs_admins';
    }
}