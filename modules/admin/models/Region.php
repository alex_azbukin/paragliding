<?php
namespace app\modules\admin\models;
use app\models\User;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

    class Region extends ActiveRecord{

        public function destinationDropdown($regions = null){

            if(is_null($regions)) {
                $regions = self::find()->orderBy('name')->all();
            }else{
                $regions = self::find()->where(['name' => $regions])->all();
            }
            $list = [];
            foreach($regions as $key => $region){
                $list[$region->id] = $region->name;
            }
            return $list;
        }

    }