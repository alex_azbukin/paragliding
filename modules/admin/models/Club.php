<?php
namespace app\modules\admin\models;
use app\models\User;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web;

use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;

class Club extends ActiveRecord{

    public $image;
    public $image_cropping;

    public static function tableName()
    {
        return 'clubs';
    }

    public function getAdmin(){
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(ClubAdmins::tableName(), ['club_id' => 'id'])
            ->orderBy(['username' => SORT_ASC]);
    }

    public function getRegion(){
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            if($this->isNewRecord){
                $this->created_at = time();
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    //copy from Cropbox extension
    public function afterSave($insert, $changedAttributes)
    {

    }

    public function createClub($form){
        $this->name = $form->name;
        $this->region_id = $form->region_id;
        $this->latitude = $form->latitude;
        $this->longitude = $form->longitude;
        $this->image = $form->image;
        $this->image_cropping = $form->image_cropping;
        if($this->save()){
            if(!$this->saveLogo($this)){
                $this->delete();
                return false;
            }
            $clubs_admins = new ClubAdmins();
            $clubs_admins->club_id = $this->id;
            $clubs_admins->user_id = $form->admin_id;
            if($clubs_admins->save()){
                return true;
            }
            return false;
        }
        return false;
    }

    public function updateData($data){
        $this->name = $data->name;
        $this->region_id = $data->region_id;
        $this->latitude = $data->latitude;
        $this->longitude = $data->longitude;
        $this->image = $data->image;
        $this->image_cropping = $data->image_cropping;
        if($this->save()){
            if($this->saveLogo($this)) {
                return true;
            }
        }
        return false;
    }

    private function saveLogo($club){
        if($club->image and isset($club->image) and $club->image_cropping and isset($club->image_cropping)) {
            $path_to_upload_dir = Yii::$app->basePath . '/web/images/clubs';

            //delete old images
            $oldImages = FileHelper::findFiles($path_to_upload_dir, [
                'only' => [
                    '/small/' . $club->id . '.*'
                ],
            ]);
            for ($i = 0; $i != count($oldImages); $i++) {
                @unlink($oldImages[$i]);
            }

            $pathThumbImage = $path_to_upload_dir. '/small/'
                . $club->id
                . '.'
                . $club->image->getExtension();

            $club->image->saveAs($pathThumbImage);
            $img = Image::crop($pathThumbImage,
                $club->image_cropping['width'],
                $club->image_cropping['height'],
                [$club->image_cropping['x'], $club->image_cropping['y']])
                ->save($pathThumbImage);
            $club->image_small_path = '/images/clubs/small/' . $club->id . '.' . $club->image->getExtension();
            if($img and $club->save()){
                return true;
            }
        }
    }

    public function clubsDropdown($club_ids = null, $region_ids = null){

        if(is_null($club_ids) and is_null($region_ids)) {
            $clubs = self::find()->orderBy('name')->all();
        }else{
            $clubs = self::find()->where(['id' => $club_ids])->orWhere(['region_id' => $region_ids])->all();
        }
        $list = [];
        foreach($clubs as $key => $club){
            $list[$club->id] = $club->name;
        }
        return $list;
    }
}