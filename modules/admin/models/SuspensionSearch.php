<?php
namespace app\modules\admin\models;

use app\models\Suspensions;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SuspensionSearch extends Suspensions{

    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes());
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Suspensions::find()->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                    'name' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'name',
                    'created_at',
                    'updated_at'
                ],
            ],
        ]);


        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'name', $this->name]);


        return $dataProvider;
    }
}
