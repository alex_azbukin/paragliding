<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\web;

class ProductForm extends Model{

    public $title;
    public $description;
    public $region_id;
    public $owner_id;
    public $type_id;
    public $id;
    public $image_cropping;
    public $image_small_path;
    public $image;
    public $galleryPhotos;
    public $photosFromGallery;
    public $manufacturer;
    public $manufacturer_url;
    public $model;
    public $size_id;
    public $year_of_issue;
    public $state;
    public $defect;
    public $sale_reason;
    public $price;
    public $min_weight;
    public $max_weight;
    public $min_growth;
    public $max_growth;
    public $color;
    public $class_id;
    public $raid;
    public $suspension_id;
    public $tread_id;
    public $equipment_id;
    public $count_of_use;

    public function rules()
    {
        return [
            [['title', 'description', 'manufacturer_url', 'defect', 'sale_reason', 'color', 'class_id', 'suspension_id', 'tread_id', 'equipment_id', 'type_id', 'count_of_use', 'size_id'], 'default', 'value' => null],
            [['title', 'description', 'region_id', 'model', 'manufacturer'], 'required'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif', 'jpeg']],
            [['galleryPhotos'], 'file', 'extensions' => 'png, jpg, gif, jpeg', 'maxFiles' => Yii::$app->params['max_upload_photos']],
            [['year_of_issue', 'state', 'price', 'min_weight', 'max_weight', 'raid', 'min_growth', 'max_growth', 'count_of_use', 'size_id', 'suspension_id', 'tread_id', 'equipment_id'], 'integer']

        ];
    }
    public function attributeLabels(){
        return [
            'title' => Yii::t('app', 'Advertisement name'),
            'description' => Yii::t('app', 'Advertisement description'),
            'image' => Yii::t('app', 'Advertisement image'),
            'region_id' => Yii::t('app', 'Region'),
            'owner_id' => Yii::t('app', 'Owner'),
            'id' => Yii::t('app', 'Id'),
            'crop_info' => Yii::t('app', 'Advertisement Image'),
            'image_small_path' => Yii::t('app', 'Current Advertisement image'),
            'galleryPhotos' => Yii::t('app', 'Photos gallery'),
            'manufacturer' => Yii::t('app', 'Advertisement Manufacturer'),
            'manufacturer_url' => Yii::t('app', 'Advertisement Manufacturer URL'),
            'model' => Yii::t('app', 'Advertisement Model'),
            'size_id' => Yii::t('app', 'Size'),
            'year_of_issue' => Yii::t('app', 'Year of issue'),
            'state' => Yii::t('app', 'State'),
            'defect' => Yii::t('app', 'Defects'),
            'sale_reason' => Yii::t('app', 'Advertisement(Sale reason)'),
            'price' => Yii::t('app', 'Advertisement Price'),
            'color' => Yii::t('app', 'Color'),
            'class_id' => Yii::t('app', 'Class'),
            'raid' => Yii::t('app', 'Raid'),
            'suspension_id' => Yii::t('app', 'Suspension'),
            'tread_id' => Yii::t('app', 'Tread'),
            'equipment_id' => Yii::t('app', 'Equipment'),
            'count_of_use' => Yii::t('app', 'Count of use'),
            'type_id' => Yii::t('app', 'Type product')
        ];
    }
}