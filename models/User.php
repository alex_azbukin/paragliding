<?php
namespace app\models;
use app\modules\admin\models\Region;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Point;

class User extends ActiveRecord implements web\IdentityInterface{

    public $auth_key;
    public $image;
    public $image_cropping;

    public static function tableName()
    {
        return 'user';
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'created_at' => Yii::t('app', 'User created at'),
            'updated_at' => Yii::t('app', 'User updated at'),
            'role_id' => Yii::t('app', 'Role')
        ];
    }
//    ----------------------//copy from Cropbox extension----------------------

    public function afterSave($insert, $changedAttributes)
    {

    }

    public function getRegion(){
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function getParapro(){
        return $this->hasOne(Parapro::className(), ['id' => 'parapro_id']);
    }

    public function getClaimPilot(){
        return $this->hasOne(ClaimPilot::className(), ['user_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['owner_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;

        //if (Yii::$app->getSession()->has('pilot-'.$id)) {
        //    return new self(Yii::$app->getSession()->get('pilot-'.$id));
        //}else{
        return static::findOne($id);
        //}
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        /*foreach (self::$users as $pilot) {
            if ($pilot['accessToken'] === $token) {
                return new static($pilot);
            }
        }
        return null;*/

        return static::findOne(['access_token' => $token]);
    }


    public static function findByUsername($username){
        return static::findByUsername(['username' => $username]);
    }

    public static function findByEmail($email)
    {
        /*foreach (self::$users as $pilot) {
            if (strcasecmp($pilot['username'], $username) === 0) {
                return new static($pilot);
            }
        }
        return null;*/
        return static::findOne(['email' => $email]);
    }

    public static  function findByEmailOrUsername($email)
    {
        return static::find()->where(['username' => $email])->orWhere(['email' => $email])->one();
    }

    public static function findByPasswordResetToken($token)
    {
        return static::findOne(['password_reset_token' => $token]);
    }

    public static function findByRegistrationToken($token)
    {
        return static::findOne(['registration_token' => $token]);
    }

    public function changeApproved()
    {
        $this->approved = 1;
        $this->registration_token = null;
        if($this->save()){
            return true;
        }
        return false;
    }

    public function inspectionApproved(){
        if($this->approved == 1){
            return true;
        }
        return false;
    }




    public function getId(){return $this->id;}

    public function getAuthKey(){return $this->auth_key;}

    public function generateAuthKey()
    {
//        $this->auth_key = md5(uniqid().\Yii::$app->params['hash']);
    }

    public function validateAuthKey($authKey){return $this->auth_key === $authKey;}

    public function setPassword($password)
    {
        $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        $this->password = $hash;
    }
//--------------------------------------------generation Token---------------------------------
    public function setResetPasswordToken()
    {
        $token = md5(uniqid());
        $this->password_reset_token = $token;
        if($this->save()){
            return true;
        }
        return false;
    }

    public function setCreateRegistrationToken()
    {
        $registration_token = md5(uniqid());
        $this->registration_token = $registration_token;
        if($this->save()){
            return true;
        }
        return false;
    }
//----------------------------------------------------------------------------------------------


    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            if($this->isNewRecord){
                $this->created_at = time();
//                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }


    public function signUp($data){
        $this->username = $data->username;
        $this->email = $data->email;
        $this->setPassword($data->password);
        //$this->image = $data->image;
        //$this->image_cropping = $data->image_cropping;
        if($this->save()){
            return true;
        }

        return false;
    }



    public function updatePassword($data){
        $this->password_reset_token = null;
        $this->setPassword($data->password);
        if($this->save()){
            return true;
        }
        return false;
    }

    public function claimPilotStatus(){
        if($this->claimPilot and isset($this->claimPilot)){
            return $this->claimPilot->status;
        }
        return -1;
    }

    public function updateUser($data){
        $this->email = $data->email;
        $this->username = $data->username;
        $this->first_name = $data->first_name;
        $this->second_name = $data->second_name;
        $this->last_name = $data->last_name;
        $this->birthday = $data->birthday;
        $this->region_id = $data->region_id;
        $this->city = $data->city;
        $this->phone = $data->phone;
        $this->description = $data->description;
        $this->participant_visits_checkbox = $data->participant_visits_checkbox;
        $this->participant_competition_checkbox = $data->participant_competition_checkbox;
        $this->ukraine_license_number = $data->ukraine_license_number;
        $this->parapro_id = $data->parapro_id;
        $this->fai_license_number = $data->fai_license_number;
        $this->license_checkbox = $data->license_checkbox;
        $this->insurance_checkbox = $data->insurance_checkbox;
        $this->fly_since = $data->fly_since;
        $this->fly_hours_amount = $data->fly_hours_amount;
        $this->average_fly_hours_amount = $data->average_fly_hours_amount;
        $this->wing = $data->wing;
        $this->reserve_parachute = $data->reserve_parachute;
        $this->helmet = $data->helmet;
        $this->devices = $data->devices;
        $this->image = $data->image;
        $this->image_cropping = $data->image_cropping;
        if($this->save()){
            if($this->saveLogo($this)) {
                return true;
            }
        }
        return false;
    }

    private function saveLogo($user){
        if($user->image and isset($user->image) and $user->image_cropping and isset($user->image_cropping)) {
            $path_to_upload_dir = Yii::$app->basePath . '/web/images/users';

            //delete old images
            $oldImages = FileHelper::findFiles($path_to_upload_dir, [
                'only' => [
                    '/small/' . $user->id . '.*'
                ],
            ]);
            for ($i = 0; $i != count($oldImages); $i++) {
                @unlink($oldImages[$i]);
            }

            $pathThumbImage = $path_to_upload_dir. '/small/'
                . $user->id
                . '.'
                . $user->image->getExtension();

            $user->image->saveAs($pathThumbImage);
            $img = Image::crop($pathThumbImage,
                $user->image_cropping['width'],
                $user->image_cropping['height'],
                [$user->image_cropping['x'], $user->image_cropping['y']])
                ->save($pathThumbImage);
            $user->image_small_path = '/images/users/small/' . $user->id . '.' . $user->image->getExtension();
            if($img and $user->save()){
                return true;
            }
        }
        return true;
    }


}
