<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Equipment extends ActiveRecord
{
    public static function tableName()
    {
        return 'equipments';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function dropdownList(){
        $equipments = self::find()->orderBy('name')->all();
        $all = [];
        foreach($equipments as $equipment){
            $all[$equipment->id] = $equipment->name;
        }
        return $all;
    }

    public function updateData($data){
        $this->name = $data->name;
        if($this->save()){
            return true;
        }
        return false;
    }
}