<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web;

class ProductFilter extends Model
{
    public $type_id;
    public $title;
    public $class_id;
    public $weight;
    public $year_of_issue;
    public $is_new;
    public $price;
    public $suspension_id;
    public $tread_id;
    public $equipment_id;
    public $count_of_use;

    public function rules(){
        return [
            [['title', 'type_id', 'price'], 'default', 'value' => null],
            [['is_new'], 'default' , 'value' => 0],
            [['weight', 'class_id', 'year_of_issue', 'weight', 'type_id', 'suspension_id', 'tread_id', 'equipment_id', 'count_of_use'], 'integer'],
            ['class_id', 'required', 'when' => function ($model) {
                        return $model->type_id == 1;
                    }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"ProductFilter[class_id]\"]').val() == 1;
            }"],
            [['suspension_id'], 'required', 'when' => function ($model) {
                return $model->type_id == 2;
            }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"ProductFilter[suspension_id]\"]').val() == 2;
            }"],
            [['tread_id'], 'required', 'when' => function ($model) {
                return $model->type_id == 2;
            }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"ProductFilter[tread_id]\"]').val() == 2;
            }"],
            [['equipment_id'], 'required', 'when' => function ($model) {
                return $model->type_id == 2;
            }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"ProductFilter[equipment_id]\"]').val() == 2;
            }"],
            ['count_of_use', 'required', 'when' => function ($model) {
                return $model->type_id == 3;
            }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"ProductFilter[count_of_use]\"]').val() == 3;
            }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Advertisement name'),
            'class_id' => Yii::t('app', 'Class'),
            'weight' => Yii::t('app', 'Weight'),
            'year_of_issue' => Yii::t('app', 'Year of issue'),
            'price' => Yii::t('app', 'Advertisement Price'),
            'raid' => Yii::t('app', 'Raid'),
            'suspension_id' => Yii::t('app', 'Suspension'),
            'tread_id' => Yii::t('app', 'Tread'),
            'equipment_id' => Yii::t('app', 'Equipment'),
            'count_of_use' => Yii::t('app', 'Count of use'),
        ];
    }
}