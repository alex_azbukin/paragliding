<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Size extends ActiveRecord
{
    public static function tableName()
    {
        return 'sizes';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function dropdownList(){
        $sizes = self::find()->orderBy('name DESC')->all();
        $all = [];
        foreach($sizes as $size){
            $all[$size->id] = $size->name;
        }
        return $all;
    }
}