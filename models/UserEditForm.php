<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web;

class UserEditForm extends Model
{
    public $id;
    public $email;
    public $username;
    public $first_name;
    public $second_name;
    public $last_name;
    public $birthday;
    public $region_id;
    public $city;
    public $phone;
    public $description;
    public $participant_visits_checkbox;
    public $participant_competition_checkbox;
    public $ukraine_license_number;
    public $parapro_id;
    public $fai_license_number;
    public $license_checkbox;
    public $insurance_checkbox;
    public $fly_since;
    public $fly_hours_amount;
    public $average_fly_hours_amount;
    public $wing;
    public $reserve_parachute;
    public $helmet;
    public $devices;
    public $image_small_path;
    public $image;
    public $image_cropping;

    public function rules(){
        return [
            ['email', 'email'],
            [['email', 'username', 'first_name', 'last_name', 'birthday', 'region_id', 'city', 'phone'], 'required'],
            [['second_name', 'description', 'ukraine_license_number', 'fai_license_number', 'fly_since', 'fly_hours_amount', 'average_fly_hours_amount', 'wing', 'reserve_parachute', 'helmet', 'devices'], 'default', 'value' => null],
            [['parapro_id'], 'default', 'value' => 1],
            [['participant_visits_checkbox', 'participant_competition_checkbox', 'license_checkbox', 'insurance_checkbox', 'image'], 'default', 'value' => 0],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif']],
        ];
    }

    public function attributeLabels(){
        return [
        'email' => Yii::t('app', 'Email'),
        'username' => Yii::t('app', 'Username'),
        'first_name' => Yii::t('app', 'First name'),
        'second_name' => Yii::t('app', 'Second name'),
        'last_name' => Yii::t('app', 'Last name'),
        'birthday' => Yii::t('app', 'Birthday'),
        'region_id' => Yii::t('app', 'Region'),
        'city' => Yii::t('app', 'City'),
        'phone' => Yii::t('app', 'Phone number'),
        'description' => Yii::t('app', 'About myself'),
        'participant_visits_checkbox' => Yii::t('app', 'Participant_visits'),
        'participant_competition_checkbox' => Yii::t('app', 'Participant_competition'),
        'ukraine_license_number' => Yii::t('app', 'Ukraine license number'),
        'parapro_id' => Yii::t('app', 'Parapro level'),
        'fai_license_number' => Yii::t('app', 'FAI license number'),
        'license_checkbox' => Yii::t('app', 'License year'),
        'insurance_checkbox' => Yii::t('app', 'Insurance'),
        'fly_since' => Yii::t('app', 'Fly since'),
        'fly_hours_amount' => Yii::t('app', 'Fly hours amount'),
        'average_fly_hours_amount' => Yii::t('app', 'Average fly hours amount'),
        'wing' => Yii::t('app', 'Wing'),
        'reserve_parachute' => Yii::t('app', 'Reserve parachute'),
        'helmet' => Yii::t('app', 'Helmet'),
        'devices' => Yii::t('app', 'Devices'),
        'image' => Yii::t('app', 'User Image'),
        'crop_info' => Yii::t('app', 'User Image'),
        'image_small_path' => Yii::t('app', 'Current user image'),
        ];
    }
}