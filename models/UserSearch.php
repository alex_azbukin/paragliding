<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id'], 'integer'],
            [['email', 'username'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes());
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find()->where(['role_id' => [1,2,3]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                    'email' => SORT_ASC,
                    'username' => SORT_ASC
                ],
                'attributes' => [
                    'id',
                    'email',
                    'username',
                    'updated_at',
                    'approved'
                ],
            ],
        ]);

        //$query->joinWith(['city']);


//        $dataProvider->sort->attributes['city.name'] = [
//            'asc' => ['city.name' => SORT_ASC],
//            'desc' => ['city.name' => SORT_DESC],
//        ];

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username]);


        return $dataProvider;
    }
}