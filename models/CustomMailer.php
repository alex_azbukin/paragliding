<?php
namespace app\models;

class CustomMailer
{
    public static function sendSingleMail($view, $subject, $to, $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['noreply'];
        }
        \Yii::$app->mailer->compose($view, $params)
        ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    public static function userRegisteredEvent($users, $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['adminEmailMain'];
        }
        $messages = [];
        foreach ($users as $user) {
            $messages[] = \Yii::$app->mailer->compose('user-registered', $params)
                ->setFrom($from)
                ->setTo($user->email)
                ->setSubject(\Yii::t('app', 'New user registered'));
        }
        \Yii::$app->mailer->sendMultiple($messages);


    }

    public static function resetPasswordEvent($to, $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['noreply'];
        }
        \Yii::$app->mailer->compose('reset-password', $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject(\Yii::t('app', 'Password reset'))
            ->send();
    }

    public static function confirmationEvent($to, $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['noreply'];
        }
        \Yii::$app->mailer->compose('confirmation', $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject(\Yii::t('app', 'Confirmation of registration'))
            ->send();
    }

    public static function claimPilotAnswer($to, $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['noreply'];
        }
        \Yii::$app->mailer->compose('claim-pilot-answer', $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject(\Yii::t('app', 'Claim to be a pilot (answer)'))
            ->send();
    }

    public static function claimPilot($users = array(), $from = null, $params = null){
        if(is_null($from)){
            $from = \Yii::$app->params['noreply'];
        }
        $messages = [];
        foreach ($users as $user) {
            $messages[] = \Yii::$app->mailer->compose('claim-pilot', $params)
                ->setFrom($from)
                ->setTo($user->email)
                ->setSubject(\Yii::t('app', 'New claim to be a pilot'));
        }
        \Yii::$app->mailer->sendMultiple($messages);
    }
}