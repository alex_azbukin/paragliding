<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Pilot extends ActiveRecord{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            if($this->isNewRecord){
                $this->created_at = time();
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }
}