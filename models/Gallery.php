<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Gallery extends ActiveRecord
{
    public static function tableName()
    {
        return 'product_gallery';
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

}