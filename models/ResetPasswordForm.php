<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web;

class ResetPasswordForm extends Model{

    public $email;

    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
            ['email', 'validateExistEmail']
        ];
    }

    public function validateExistEmail($attribute, $params)
    {
        if (!User::findByEmail($this->email)) {
            $this->addError($attribute, Yii::t('app', 'This email address does not exist.'));
        }
    }

}