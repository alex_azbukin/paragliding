<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Parapro extends ActiveRecord
{
    public static function tableName()
    {
        return 'parapro';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function levelsList(){
        $parapros = self::find()->orderBy('name')->all();
        $levels = [];
        foreach($parapros as $level){
            $levels[$level->id] = $level->name;
        }
        return $levels;
    }
}