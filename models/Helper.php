<?php
namespace app\models;
use yii\helpers\Html;

class Helper
{
    public static function getImage($path_to_image = null, $type = 'product', $class = ''){
        if(!is_null($path_to_image)){
            $img = Html::img($path_to_image, ['class' => $class]);
        }
        else{
            switch($type){
                case 'user':
                    $img = Html::img(\Yii::$app->params['empty_user_photo'], ['class' => $class]);
                    break;
                case 'product':
                    $img = Html::img(\Yii::$app->params['empty_product_photo'], ['class' => $class]);
                    break;
                default:
                    $img = Html::img(\Yii::$app->params['empty_photo'], ['class' => $class]);
                    break;
            }

        }
        return $img;
    }

    public static function validateStringField($string = null){
        if(!is_null($string)){
            return $string;
        }
        return '';
    }

    public static function yearList($to = null){
        if(is_null($to)){
            $to = 1980;
        }
        $from = date('Y');
        $years = [];
        for($i = $from; $i > $to; $i--){
            $years[$i] = $i;
        }
        return $years;
    }
}