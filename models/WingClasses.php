<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class WingClasses extends ActiveRecord
{
    public static function tableName()
    {
        return 'wing_classes';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function dropdownList(){
        $classes = self::find()->orderBy('short_name DESC')->all();
        $all = [];
        foreach($classes as $class){
            $all[$class->id] = $class->short_name;
        }
        return $all;
    }
}