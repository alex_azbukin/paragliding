<?php
namespace app\models;
use app\modules\admin\models\Region;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web;

use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Point;

class Product extends ActiveRecord{

    const ANOTHER = 0;
    const WING = 1;
    const HARNESS = 2;
    const RESERVE_PARACHUTE = 3;

    public $image;
    public $image_cropping;
    public $galleryPhotos;

    public static function tableName()
    {
        return 'product';
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            if($this->isNewRecord){
                $this->created_at = time();
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            BaseFileHelper::removeDirectory(Yii::$app->basePath . '/web/images/galleries/products/' . $this->id);
            Gallery::deleteAll(['product_id' => $this->id]);
            return true;
        }
        return false;
    }


    public function getRegion(){
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    public function getSize()
    {
        return $this->hasOne(Size::className(), ['id' => 'size_id']);
    }
    public function getWingClass()
    {
        return $this->hasOne(WingClasses::className(), ['id' => 'class_id']);
    }
    public function getSuspension()
    {
        return $this->hasOne(Suspensions::className(), ['id' => 'suspension_id']);
    }
    public function getTread()
    {
        return $this->hasOne(Tread::className(), ['id' => 'tread_id']);
    }
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }

    public function getPhotosFromGallery(){
        return $this->hasMany(Gallery::className(), ['product_id' => 'id']);
    }


    public function createProduct($form){
        $this->title = $form->title;
        $this->region_id = $form->region_id;
        $this->description = $form->description;
        $this->image = $form->image;
        $this->image_cropping = $form->image_cropping;
        $this->type_id = $form->type_id;
        $this->manufacturer = $form->manufacturer;
        $this->manufacturer_url = $form->manufacturer_url;
        $this->model = $form->model;
        $this->size_id = $form->size_id;
        $this->year_of_issue = $form->year_of_issue;
        $this->state = $form->state;
        $this->defect = $form->defect;
        $this->sale_reason = $form->sale_reason;
        $this->price = $form->price;
        $this->min_weight = $form->min_weight;
        $this->max_weight = $form->max_weight;
        $this->min_growth = $form->min_growth;
        $this->max_growth = $form->max_growth;
        $this->color = $form->color;
        $this->class_id = $form->class_id;
        $this->raid = $form->raid;
        $this->suspension_id = $form->suspension_id;
        $this->tread_id = $form->tread_id;
        $this->equipment_id = $form->equipment_id;
        $this->count_of_use = $form->count_of_use;
        $this->owner_id = Yii::$app->user->identity->getId();
        if($this->save()){
            if(!$this->saveLogo($this)){
                return false;
            }
            //save product`s gallery photos
            $gallery_dir_path = Yii::$app->basePath . '/web/images/galleries/products/' . $this->id;
            if(!is_dir($gallery_dir_path)){
                BaseFileHelper::createDirectory($gallery_dir_path, 0755);
            }
            if(count($form->galleryPhotos)){
                foreach($form->galleryPhotos as $photo){
                    $photo_file_path = $gallery_dir_path . '/' . $photo->name;
                    $photo_database_path = '/images/galleries/products/' . $this->id . '/' . $photo->name;
                    if($photo->saveAs($photo_file_path)) {
                        $gallery = new Gallery();
                        $gallery->path = $photo_database_path;
                        $gallery->product_id = $this->id;
                        $gallery->save();
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function updateData($data){
        $this->title = $data->title;
        $this->region_id = $data->region_id;
        $this->description = $data->description;
        $this->image = $data->image;
        $this->image_cropping = $data->image_cropping;
        //$this->owner_id = (!isset($data->owner_id)) ? Yii::$app->user->identity->getId() : $data->owner_id;
        $this->type_id = $data->type_id;
        if($this->save()){
            if(!$this->saveLogo($this)) {
                return false;
            }
            //save product`s gallery photos
            $gallery_dir_path = Yii::$app->basePath . '/web/images/galleries/products/' . $this->id;
            if(!is_dir($gallery_dir_path)){
                BaseFileHelper::createDirectory($gallery_dir_path, 0755);
            }
            if(count($data->galleryPhotos)){
                foreach($data->galleryPhotos as $photo){
                    $photo_file_path = $gallery_dir_path . '/' . $photo->name;
                    $photo_database_path = '/images/galleries/products/' . $this->id . '/' . $photo->name;
                    if($photo->saveAs($photo_file_path)) {
                        $gallery = new Gallery();
                        $gallery->path = $photo_database_path;
                        $gallery->product_id = $this->id;
                        $gallery->save();
                    }
                }
            }
            return true;
        }
        return false;
    }

    private function saveLogo($product){
        if($product->image and isset($product->image) and $product->image_cropping and isset($product->image_cropping)) {
            $path_to_upload_dir = Yii::$app->basePath . '/web/images/products';

            //delete old images
            $oldImages = FileHelper::findFiles($path_to_upload_dir, [
                'only' => [
                    '/small/' . $product->id . '.*'
                ],
            ]);
            for ($i = 0; $i != count($oldImages); $i++) {
                @unlink($oldImages[$i]);
            }

            $pathThumbImage = $path_to_upload_dir. '/small/'
                . $product->id
                . '.'
                . $product->image->getExtension();

            $product->image->saveAs($pathThumbImage);
            $img = Image::crop($pathThumbImage,
                $product->image_cropping['width'],
                $product->image_cropping['height'],
                [$product->image_cropping['x'], $product->image_cropping['y']])
                ->save($pathThumbImage);
            $product->image_small_path = '/images/products/small/' . $product->id . '.' . $product->image->getExtension();
            if($img and $product->save()){
                return true;
            }
        }
        return true;
    }

    public function updateShowCounter(){

    }

}
