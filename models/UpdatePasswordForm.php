<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web;

    class UpdatePasswordForm extends Model{

        public $password;
        public $password_repeat;
        public $token;

        public  function rules(){
            return [
              [['password', 'password_repeat'], 'required'],
              ['password', 'compare'],
              ['token', 'default', 'value' => null]
            ];
        }

        public function attributeLabels()
        {
            return [
                'password' => Yii::t('app', 'Password_new'),
                'password_repeat' => Yii::t('app', 'Password repeat'),
            ];
        }
    }