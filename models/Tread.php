<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Tread extends ActiveRecord
{
    public static function tableName()
    {
        return 'treads';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function dropdownList(){
        $treads = self::find()->orderBy('name')->all();
        $all = [];
        foreach($treads as $tread){
            $all[$tread->id] = $tread->name;
        }
        return $all;
    }
}