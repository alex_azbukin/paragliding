<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class ClaimPilot extends ActiveRecord
{
    public static function tableName()
    {
        return 'claim_pilot';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }
}