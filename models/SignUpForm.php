<?php
namespace app\models;

use Yii;
use yii\web;
use yii\base\Model;
use yii\behaviors;

class SignUpForm extends Model{

    public $username;
    public $password;
    public $password_repeat;
    public $email;

    public function rules(){
        return[
            [['username', 'password', 'password_repeat', 'email'], 'required'],
            ['password','compare'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'message' => Yii::t('app','{attribute} has already been taken')],
            ['username', 'unique', 'targetClass' => '\app\models\User',
                'message' => Yii::t('app','{attribute} has already been taken')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Password repeat'),
        ];
    }

}
