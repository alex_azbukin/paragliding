<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ClubSearchForm extends Model
{
    public $region_id;
    public $club_id;

    public function rules()
    {
        return [
            [['region_id', 'club_id'], 'default', 'value' => null]
        ];
    }

    public function attributeLabels(){
        return [
            'club_id' => Yii::t('app', 'Club name'),
            'region_id' => Yii::t('app', 'Region'),
        ];
    }
}