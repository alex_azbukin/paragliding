<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web;

class Suspensions extends ActiveRecord
{
    public static function tableName()
    {
        return 'suspensions';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->updated_at = time();
            }
            return true;
        }
        return false;
    }

    public function dropdownList(){
        $suspensions = self::find()->orderBy('name')->all();
        $all = [];
        foreach($suspensions as $suspension){
            $all[$suspension->id] = $suspension->name;
        }
        return $all;
    }

    public function updateData($data){
        $this->name = $data->name;
        if($this->save()){
            return true;
        }
        return false;
    }
}