<?php

namespace app\controllers;

use app\models\CommentForm;
use app\models\Equipment;
use app\models\ProductFilter;
use app\models\Size;
use app\models\Suspensions;
use app\models\Tread;
use app\models\WingClasses;
use Yii;
use app\models\Product;
use app\models\User;
use app\models\ProductSearch;
use app\modules\admin\models\ProductForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\modules\admin\models\Region;
use yii\web;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\imagine\Image;

use yii\data\Pagination;

class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create-product', 'edit-product'],
                'rules' => [
                    [
                        'actions' => ['create-product', 'edit-product'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $productFilter = new ProductFilter();
        $wingClasses = new WingClasses();
        $suspension = new Suspensions();
        $tread = new Tread();
        $equipment = new Equipment();
        $classesList = $wingClasses->dropdownList();
        $suspensionList = $suspension->dropdownList();
        $treadsList = $tread->dropdownList();
        $equipmentList = $equipment->dropdownList();
        $products['wings'] = Product::find()->where(['type_id' => Product::WING])->orderBy('created_at DESC')->all();
        $products['harness'] = Product::find()->where(['type_id' => Product::HARNESS])->orderBy('created_at DESC')->all();
        $products['reserve_parachute'] = Product::find()->where(['type_id' => Product::RESERVE_PARACHUTE])->orderBy('created_at DESC')->all();
        $products['another'] = Product::find()->where(['type_id' => Product::ANOTHER])->orderBy('created_at DESC')->all();
        return $this->render('index', [
            'products' => $products,
            'productFilter' => $productFilter,
            'classesList' => $classesList,
            'suspensionList' => $suspensionList,
            'treadsList' => $treadsList,
            'equipmentList' => $equipmentList

        ]);
    }

//    -----------------------------Create Product-----------------------------------

    public function actionCreateProduct(){
        $form = new ProductForm();
        $region = new Region();
        $sizes = new Size();
        $wingClasses = new WingClasses();
        $suspension = new Suspensions();
        $tread = new Tread();
        $equipment = new Equipment();
        $destinationDropdown = $region->destinationDropdown();
        $sizesList = $sizes->dropdownList();
        $classesList = $wingClasses->dropdownList();
        $suspensionList = $suspension->dropdownList();
        $treadsList = $tread->dropdownList();
        $equipmentList = $equipment->dropdownList();

        if($form->load(Yii::$app->request->post()) and $form->validate()) {
            $model = new Product();
            $form->image = UploadedFile::getInstance($form, 'image');
            $form->image_cropping = Yii::$app->request->post('image-cropping');
            $form->galleryPhotos = UploadedFile::getInstances($form, 'galleryPhotos');
            if ($model->createProduct($form)) {
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/user/products']);
            }
            return $this->render('create-product', [
                'model' => $form,
                'destinationDropdown' => $destinationDropdown,
                'sizesList' => $sizesList,
                'classesList' => $classesList,
                'suspensionList' => $suspensionList,
                'treadsList' => $treadsList,
                'equipmentList' => $equipmentList
            ]);
        }

        return $this->render('create-product',[
            'model' => $form,
            'destinationDropdown' => $destinationDropdown,
            'sizesList' => $sizesList,
            'classesList' => $classesList,
            'suspensionList' => $suspensionList,
            'treadsList' => $treadsList,
            'equipmentList' => $equipmentList
        ]);
    }

//    -----------------------------Edit Product-------------------------------------

    public function actionEditProduct($id)
    {
        $form = new ProductForm();
        $region = new Region();
        $sizes = new Size();
        $wingClasses = new WingClasses();
        $suspension = new Suspensions();
        $tread = new Tread();
        $equipment = new Equipment();
        $sizesList = $sizes->dropdownList();
        $classesList = $wingClasses->dropdownList();
        $suspensionList = $suspension->dropdownList();
        $treadsList = $tread->dropdownList();
        $equipmentList = $equipment->dropdownList();
        $dropdownRegions = $region->destinationDropdown();
        if (Yii::$app->request->isPost and ($form->load(Yii::$app->request->post())) and $form->validate()) {
            $product = Product::findOne($id);
            $form->image = UploadedFile::getInstance($form, 'image');
            $form->image_cropping = Yii::$app->request->post('image-cropping');
            $form->galleryPhotos = UploadedFile::getInstances($form, 'galleryPhotos');
            if ($product->updateData($form)) {
                Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
                Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
                return $this->redirect(['/user/products']);
            }

            Yii::$app->session->setFlash('adminBoardPageClass', 'alert-success');
            Yii::$app->session->setFlash('adminBoardPageMessage', Yii::t('app', 'Data was saved successful'));
            return $this->redirect(['/user/products ']);
        }
        else{
            $product = Product::findOne($id);
            $form->image_small_path = $product->image_small_path;
            $form->title = $product->title;
            $form->description = $product->description;
            $form->type_id = $product->type_id;
            $form->region_id = $product->region_id;
            $form->owner_id = $product->owner_id;
            $form->id = $product->id;
            $form->photosFromGallery = $product->photosFromGallery;
            $form->manufacturer = $product->manufacturer;
            $form->manufacturer_url = $product->manufacturer_url;
            $form->model = $product->model;
            $form->size_id = $product->size_id;
            $form->year_of_issue = $product->year_of_issue;
            $form->state = $product->state;
            $form->defect = $product->defect;
            $form->sale_reason = $product->sale_reason;
            $form->price = $product->price;
            $form->min_weight = $product->min_weight;
            $form->max_weight = $product->max_weight;
            $form->min_growth = $product->min_growth;
            $form->max_growth = $product->max_growth;
            $form->color = $product->color;
            $form->class_id = $product->class_id;
            $form->raid = $product->raid;
            $form->suspension_id = $product->suspension_id;
            $form->tread_id = $product->tread_id;
            $form->equipment_id = $product->equipment_id;
            $form->count_of_use = $product->count_of_use;


            return $this->render('edit-product', [
                'model' => $form,
                'dropdownRegions' => $dropdownRegions,
                'sizesList' => $sizesList,
                'classesList' => $classesList,
                'suspensionList' => $suspensionList,
                'treadsList' => $treadsList,
                'equipmentList' => $equipmentList
            ]);
        }
    }

//----------------------------------Delete Product----------------------------------


    public function actionDeleteProduct($id){
        if(Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $product = Product::findOne($id);
            if($product->delete()){
                $response = [
                    'status' => 'ok',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Product was deleted successful'),
                    'type' => 'products'
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'element_id' => $id,
                    'msg' => Yii::t('app', 'Error. Product wasn`t deleted'),
                    'type' => 'products'
                ];
            }
            echo Json::encode($response);
        }
    }


//----------------------------------------------------------------------------------

    public function actionViewProduct($id){
        if(!is_null($id)){
            $product = Product::findOne($id);
            $product->updateCounters(['show_counter' => 1]);
            $commentForm = new CommentForm();
            return $this->render('view-product', [
                'product' => $product,
                'comment' => $commentForm
            ]);
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFilterProduct(){
        if(Yii::$app->request->isAjax) {
            $productFilter = new ProductFilter();
            if($productFilter->load(Yii::$app->request->post()) and $productFilter->validate()){
                $products = array();
                (!is_null($productFilter->title)) ? $title = $productFilter->title : $title = '';
                switch($productFilter->type_id){
                    case Product::WING:
                    $products = Product::find()
                        ->where([
                            'type_id' => $productFilter->type_id,
                            'is_new' => $productFilter->is_new
                        ])
                        ->andFilterWhere(['LIKE', 'title', $title])
                        ->orderBy('created_at DESC')
                        ->all();
                        break;
                    case Product::HARNESS:
                    $products = Product::find()
                        ->where([
                            'type_id' => $productFilter->type_id,
                            'is_new' => $productFilter->is_new
                        ])
                        ->andFilterWhere(['LIKE', 'title', $title])
                        ->orderBy('created_at DESC')
                        ->all();
                        break;
                    case Product::RESERVE_PARACHUTE:
                    $products = Product::find()
                        ->where([
                            'type_id' => $productFilter->type_id,
                            'is_new' => $productFilter->is_new
                        ])
                        ->andFilterWhere(['LIKE', 'title', $title])
                        ->orderBy('created_at DESC')
                        ->all();
                        break;
                    default:
                    $products = Product::find()
                        ->where([
                            'type_id' => $productFilter->type_id,
                            'is_new' => $productFilter->is_new
                        ])
                        ->andFilterWhere(['LIKE', 'title', $title])
                        ->orderBy('created_at DESC')
                        ->all();
                        break;
                }
                if(is_array($products)) {
                    $response = [
                        'status' => 'ok',
                        'type_id' => $productFilter->type_id,
                        'html' => $this->renderAjax('//product/partial/product-list', ['products' => $products, 'type_id' => $productFilter->type_id]),
                        'model' => $productFilter
                    ];
                }
                else{
                    $response = [
                        'status' => 'errors',
                        'type_id' => $productFilter->type_id,
                        'message' => Yii::t('app-error', 'Advertisement were not find. Please, change search condition')
                    ];
                }
            }
            else{
                $response = [
                    'status' => 'get-errors',
                    'errors' => $productFilter->getErrors(),
                    'message' => Yii::t('app-error', 'Advertisement were not find. Please, change search condition')
                ];
            }
        }
        else{
            $response = [
                'status' => 'error',
                'message' => Yii::t('app-error', 'Current request is not allow')
            ];
        }
        echo Json::encode($response);
        Yii::$app->end();
    }
}
