<?php
namespace app\controllers;

use app\models\Parapro;
use app\models\Product;
use app\models\User;
use app\models\UserEditForm;
use app\modules\admin\models\Region;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\imagine\Image;
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit'],
                'rules' => [
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionProfile($id = null){
        if(!is_null($id)) {
            $user = User::findOne($id);
        }
        else{
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
        }
        return $this->render('profile', ['user' => $user]);
    }

    public function actionEdit(){
        $model = new UserEditForm();
        $region = new Region();
        $parapro = new Parapro();
        $dropdownRegions = $region->destinationDropdown();
        $levelsList = $parapro->levelsList();
        if($model->load(Yii::$app->request->post()) and $model->validate()){
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image_cropping = Yii::$app->request->post('image-cropping');
            if($user and isset($user)){
                if($user->updateUser($model)){
                    Yii::$app->session->setFlash('profilePageClass', 'alert-success');
                    Yii::$app->session->setFlash('profilePageMessage', Yii::t('app', 'Data was saved successful'));
                    return $this->redirect(['/user/profile']);
                }
                return $this->render('edit-profile', [
                    'model' => $model,
                    'dropdownRegions' => $dropdownRegions,
                    'levelsList' => $levelsList
                ]);
            }
        }
        else {
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            if ($user and isset($user)) {
                $model->image_small_path = $user->image_small_path;
                $model->email = $user->email;
                $model->username = $user->username;
                $model->first_name = $user->first_name;
                $model->second_name = $user->second_name;
                $model->last_name = $user->last_name;
                $model->birthday = $user->birthday;
                $model->region_id = $user->region_id;
                $model->city = $user->city;
                $model->phone = $user->phone;
                $model->description = $user->description;
                $model->participant_visits_checkbox = $user->participant_visits_checkbox;
                $model->participant_competition_checkbox = $user->participant_competition_checkbox;
                $model->ukraine_license_number = $user->ukraine_license_number;
                $model->parapro_id = $user->parapro_id;
                $model->fai_license_number = $user->fai_license_number;
                $model->license_checkbox = $user->license_checkbox;
                $model->insurance_checkbox = $user->insurance_checkbox;
                $model->fly_since = $user->fly_since;
                $model->fly_hours_amount = $user->fly_hours_amount;
                $model->average_fly_hours_amount = $user->average_fly_hours_amount;
                $model->wing = $user->wing;
                $model->reserve_parachute = $user->reserve_parachute;
                $model->helmet = $user->helmet;
                $model->devices = $user->devices;
                return $this->render('edit-profile', [
                    'model' => $model,
                    'dropdownRegions' => $dropdownRegions,
                    'levelsList' => $levelsList
                ]);
            }
            return $this->goHome();
        }
        return $this->render('edit-profile', [
            'model' => $model,
            'dropdownRegions' => $dropdownRegions,
            'levelsList' => $levelsList
        ]);
    }

    public function actionProducts($id = null){
        $products = Product::find()
            ->where(['owner_id' => (!is_null($id)) ? $id : Yii::$app->user->identity->getId()])
            ->orderBy('created_at DESC')
            ->all();
        return $this->render('products', [
            'products' => $products
        ]);
    }
}