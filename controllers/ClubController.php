<?php namespace app\controllers;

use app\models\ClubSearchForm;
use app\modules\admin\models\Club;
use app\modules\admin\models\Region;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;

class ClubController extends Controller
{
    public function actionIndex(){
        $club = new Club();
        $region = new Region();
        $clubSearchForm = new ClubSearchForm();
        $clubs = Club::find()->orderBy('name')->all();
        $clubsDropdown = $club->clubsDropdown();
        $regionDropdown = $region->destinationDropdown();
        return $this->render('index', [
            'clubs' => $clubs,
            'model' => $clubSearchForm,
            'clubsDropdown' => $clubsDropdown,
            'regionDropdown' => $regionDropdown
        ]);
    }

    public function actionAjaxSearch(){
        if(Yii::$app->request->isAjax){
            $searchForm = new ClubSearchForm();
            if($searchForm->load(Yii::$app->request->get())){
                $clubs_arr = Club::find()->where(['id' => $searchForm->club_id])->orWhere(['region_id' => $searchForm->region_id])->all();
                $clubs = [];
                foreach($clubs_arr as $key => $club){
                    $clubs[$key]['club_id'] = $club->id;
                    $clubs[$key]['name'] = $club->name;
                    $clubs[$key]['logo'] = $club->image_small_path;
                    $clubs[$key]['region']['id'] = $club->region_id;
                    $clubs[$key]['region']['name'] = $club->region->name;
                    $clubs[$key]['admin']['id'] = $club->admin[0]->id;
                    $clubs[$key]['admin']['name'] = $club->admin[0]->first_name.' '.$club->admin[0]->last_name;
                    $clubs[$key]['admin']['avatar'] = isset($club->admin[0]->image_small_path) ? $club->admin[0]->image_small_path : Yii::$app->params['empty_user_photo'];
                    $clubs[$key]['latitude'] = $club->latitude;
                    $clubs[$key]['longitude'] = $club->longitude;
                }
                if(count($clubs)) {
                    $response = [
                        'status' => 'ok',
                        'message' => Yii::t('app','Success'),
                        'clubs' => $clubs
                    ];
                }
                else{
                    $response = [
                        'status' => 'error',
                        'message' => Yii::t('app-error', 'Clubs were not find. Please, change search condition')
                    ];
                }
            }
            else{
                $response = [
                    'status' => 'error',
                    'message' => Yii::t('app-error', 'Clubs were not find. Please, change search condition')
                ];
            }
        }
        else{
            $response = [
                'status' => 'error',
                'message' => Yii::t('app-error', 'Current request is not allow')
            ];
        }
        echo Json::encode($response);
        Yii::$app->end();
    }
}