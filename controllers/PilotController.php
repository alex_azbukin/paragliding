<?php namespace app\controllers;


use app\components\EventHandler;
use app\components\MailerEvent;
use app\models\ClaimPilot;
use app\models\Parapro;
use app\models\PilotForm;
use app\models\User;
use app\modules\admin\models\Region;
use Yii;
use yii\base\Model;
use yii\web\Controller;


class PilotController extends Controller{

    public function actionAddPilot(){
        $model = new PilotForm();
        $region = new Region();
        $parapro = new Parapro();
        $dropdownRegions = $region->destinationDropdown();
        $levelsList = $parapro->levelsList();
        if($model->load(Yii::$app->request->post()) && $model->validate() ){
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            if($user and isset($user)){
                $user->first_name = $model->first_name;
                $user->second_name = $model->second_name;
                $user->last_name = $model->last_name;
                $user->birthday = $model->birthday;
                $user->region_id = $model->region_id;
                $user->city = $model->city;
                $user->phone = $model->phone;
                $user->description = $model->description;
                $user->participant_visits_checkbox = $model->participant_visits_checkbox;
                $user->participant_competition_checkbox = $model->participant_competition_checkbox;
                $user->ukraine_license_number = $model->ukraine_license_number;
                $user->parapro_id = $model->parapro_id;
                $user->fai_license_number = $model->fai_license_number;
                $user->license_checkbox = $model->license_checkbox;
                $user->insurance_checkbox = $model->insurance_checkbox;
                $user->fly_since = $model->fly_since;
                $user->fly_hours_amount = $model->fly_hours_amount;
                $user->average_fly_hours_amount = $model->average_fly_hours_amount;
                $user->wing = $model->wing;
                $user->reserve_parachute = $model->reserve_parachute;
                $user->helmet = $model->helmet;
                $user->devices = $model->devices;
                if($user->save()){
                    $claimPilot = new ClaimPilot();
                    $claimPilot->user_id = $user->id;
                    $eventHandler = new EventHandler();
                    $mailerEvent = new MailerEvent();
                    $mailerEvent->user = $user;
                    $eventHandler->on(EventHandler::EVENT_CLAIM_PILOT, [$mailerEvent, 'claimPilot']);
                    if($claimPilot->save()) {
                        $eventHandler->claimPilot();
                        Yii::$app->session->setFlash('profilePageClass', 'alert-success');
                        Yii::$app->session->setFlash('profilePageMessage', Yii::t('app', 'Claim was submit successful'));
                    }
                    else{
                        Yii::$app->session->setFlash('profilePageClass', 'alert-danger');
                        Yii::$app->session->setFlash('profilePageMessage', Yii::t('app', 'Please, submit form once again'));
                    }
                    return $this->redirect(['/user/profile']);
                }
                return $this->render('add-pilot', [
                    'model' => $model,
                    'dropdownRegions' => $dropdownRegions,
                    'levelsList' => $levelsList
                ]);
            }
        }
        else{
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            if ($user and isset($user)) {
                $model->first_name = $user->first_name;
                $model->second_name = $user->second_name;
                $model->last_name = $user->last_name;
                $model->birthday = $user->birthday;
                $model->region_id = $user->region_id;
                $model->city = $user->city;
                $model->phone = $user->phone;
                $model->description = $user->description;
                $model->participant_visits_checkbox = $user->participant_visits_checkbox;
                $model->participant_competition_checkbox = $user->participant_competition_checkbox;
                $model->ukraine_license_number = $user->ukraine_license_number;
                $model->parapro_id = $user->parapro_id;
                $model->fai_license_number = $user->fai_license_number;
                $model->license_checkbox = $user->license_checkbox;
                $model->insurance_checkbox = $user->insurance_checkbox;
                $model->fly_since = $user->fly_since;
                $model->fly_hours_amount = $user->fly_hours_amount;
                $model->average_fly_hours_amount = $user->average_fly_hours_amount;
                $model->wing = $user->wing;
                $model->reserve_parachute = $user->reserve_parachute;
                $model->helmet = $user->helmet;
                $model->devices = $user->devices;
            }
            return $this->render('add-pilot',[
                'model' => $model,
                'dropdownRegions' => $dropdownRegions,
                'levelsList' => $levelsList
            ]);
        }
        return $this->render('add-pilot',[
            'model' => $model,
            'dropdownRegions' => $dropdownRegions,
            'levelsList' => $levelsList
        ]);
    }

    public function actionIndex(){
        return $this->render('index', []);
    }
}