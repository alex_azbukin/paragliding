<?php

namespace app\controllers;

use app\components\EventHandler;
use app\components\MailerEvent;
use app\models\UpprovedForm;
use app\models\User;
use app\models\SignUpForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ResetPasswordForm;
use app\models\UpdatePasswordForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findByEmailOrUsername($model->email);
            if($user->inspectionApproved()){
                $model->login();
                return $this->goBack();
            }
            else{
                Yii::$app->session->setFlash('loginPageClass', 'alert-danger');
                Yii::$app->session->setFlash('loginPageMessage', Yii::t('app', 'Excuse me. You need to confirm your registration'));
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }



    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignUp(){
        $form = new SignUpForm();
        if($form->load(Yii::$app->request->post()) && $form->validate()){
            $user = new User();
            if($user->signUp($form) && $user->setCreateRegistrationToken($form)){
                $eventHandler = new EventHandler();
                $mailerEvent = new MailerEvent();
                $mailerEvent->user = $user;
                $eventHandler->on(EventHandler::EVENT_CONFIRMATION, [$mailerEvent, 'confirmation']);
                $eventHandler->on(EventHandler::EVENT_USER_REGISTERED, [$mailerEvent, 'userRegistered']);
                $eventHandler->userRegistered();
                $eventHandler->confirmation();
                return $this->redirect('/site/ok');
            }
            return $this->render('sign-up', ['model' => $form]);
        }
        return $this->render('sign-up', ['model' => $form]);
    }



        public  function actionOk(){
            return $this->render('ok', []);
        }


        public function actionResetPassword(){
            $form = new ResetPasswordForm();
            if($form->load(Yii::$app->request->post()) && $form->validate()){
                $user = User::findByEmail($form->email);
                if($user->setResetPasswordToken($form)){
                    $eventHandler = new EventHandler();
                    $mailerEvent = new MailerEvent();
                    $mailerEvent->user = $user;
                    $eventHandler->on(EventHandler::EVENT_RESET_PASSWORD, [$mailerEvent, 'resetPassword']);
                    $eventHandler->resetPassword();
                    return $this->redirect('/site/information-window');
                }
            }
            return $this->render('/site/reset-password', ['model' => $form]);
        }


        public function actionUpdatePassword(){
            $form = new UpdatePasswordForm();
            if($form->load(Yii::$app->request->post()) && $form->validate()){
                $user= User::findByPasswordResetToken($form->token);
                if($user->updatePassword($form)){
                    Yii::$app->session->setFlash('loginPageClass', 'alert-success');
                    Yii::$app->session->setFlash('loginPageMessage', Yii::t('app', 'Now you can login with your new password'));
                    return $this->redirect('/site/login');
                }
            }
            return $this->render('update-password', [
                'model' => $form
            ]);
        }

        public function actionInformationWindow(){
            return $this->render('information-window', []);
        }

    public function actionApproved(){
        $registration_token = Yii::$app->request->get('registration_token');
        $user = User::findByRegistrationToken($registration_token);
            if($user){
                $user->changeApproved();
                Yii::$app->session->setFlash('loginPageClass', 'alert-success');
                Yii::$app->session->setFlash('loginPageMessage', Yii::t('app', 'You account has been approved successful'));
            }
        return $this->redirect('/site/login');
        }


}
