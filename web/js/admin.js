$(function(){
// search users for club
    $('#clubform-admin_name').on('input', function(){
        var start_length = 2;
        var value = $(this).val();
        var url = $('.search-admin').attr('href');
        $('.club-admins').remove();
        if(value.length > start_length) {
            $.get(url, {'user_name_or_email' : value}).done(function(response){
                var data = $.parseJSON(response);
                if(data.status == 'ok'){
                    var list = '<ul class="club-admins">';
                    for(var i in data.users){
                        list += '<li value="' + i + '" name="' + data.users[i] + '" class="admin-for-club">' + data.users[i] + '</li>';
                    }
                    list += '</ul>';
                    $('#clubform-admin_name').after(list);
                }
                else{
                    $('#clubform-admin_name').after('');
                    alert(data.message);
                }
            }).fail(function(response){
                alert(response.responseText);
            });
        }
    });

    //add delete row from admin
    $('.admin-panel .delete, .site .delete').on('click', function(event){
        event.preventDefault();
        var url = $(this).attr('href');

        var confirm_delete = confirm('Вы действительно хотите удалить этот элемент?');

        if(confirm_delete) {

            $.post(url).done(function (answer) {
                var data = $.parseJSON(answer);
                console.log(data);
                if (data.status == 'ok') {
                    $('.' + data.type +'[data-element-id="' + data.element_id +'"]').fadeOut(2000);
                } else {
                    alert(data.msg);
                }
            }).fail(function (answer) {
                alert('error');
            });
        }
    });




    //add admin ID to input in form
    $(document).on('click', '.admin-for-club', function(){
        var value = $(this).attr('value');
        var name = $(this).attr('name');
        $('[name="ClubForm[admin_id]"]').val(value);
        $('[name="ClubForm[admin_name]"]').val(name);
        $('.club-admins').remove();
    });
});