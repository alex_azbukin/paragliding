$(function(){
    window.MapZoomDefault = 6;
    window.MapLatitudeDefault = 48.00000;
    window.MapLongitudeDefault = 33.00000;
    //search club in club`s list
    $('#search-club').on('click', function(e){
        e.preventDefault();
        var form = $('#club-list');
        var data = form.serialize();
        var url = form.attr('action');
        console.log(url);
        $.get(url, data)
            .done(function(response){
                var data = $.parseJSON(response);
                if(data.status == 'ok'){
                    var myLatLng = {lat: window.MapLatitudeDefault, lng: window.MapLongitudeDefault};
                    var map = new google.maps.Map(document.getElementById('map-canvas'), {
                        zoom: window.MapZoomDefault,
                        center: myLatLng
                    });
                    var result_html = '';
                    for(var i in data.clubs){
                        console.log(data.clubs[i]);
                        var image = {
                            url: data.clubs[i].logo,
                            // This marker is 20 pixels wide by 20 pixels high.
                            size: new google.maps.Size(50, 50),
                            // The origin for this image is (0, 0).
                            origin: new google.maps.Point(0, 0),
                            // The anchor for this image is the base of the flagpole at (0, 32).
                            anchor: new google.maps.Point(0, 32)
                        };
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(data.clubs[i].latitude, data.clubs[i].longitude),
                            map: map,
                            title: data.clubs[i].name,
                            icon: image,
                            club_id: data.clubs[i].club_id
                        });
                        marker.setMap(map);
                        marker.addListener('click', function() {
                            alert('Title : '+this.title);
                        });
                        result_html += '<div class="col-xs-12 club-search-row"> <div class="col-xs-2"> Title : ' + data.clubs[i].name + '</div>' +
                        '<div class="col-xs-2">Club photo : <img src="' + data.clubs[i].logo + '" class="img-responsive" style="width:70px;"></div>' +
                        '<div class="col-xs-2">Region : ' + data.clubs[i].region.name + '</div>' +
                        '<div class="col-xs-2">Admin : ' + data.clubs[i].admin.name + '</div>' +
                        '<div class="col-xs-2">Admin photo : <img src="' + data.clubs[i].admin.avatar + '" class="img-responsive" style="width:70px;"></div>' +
                        '<div class="col-xs-2">Latitude : ' + data.clubs[i].latitude + ' , Longitude : ' + data.clubs[i].longitude + '</div></div>';
                    }
                    $('.search-result').html(result_html);
                }
                else{
                    alert(data.message);
                }
            })
            .fail(function(jqxhr, settings, thrownError){
                alert(thrownError);
            });
    });

    //switch advertisement type
    $('#productform-type_id').on('change', function(){
        var value = $(this).val();
        $('form .advertisement').removeClass('show').addClass('hidden');
        $('form .advertisement.attributes-' + value).addClass('show').removeClass('hidden');
    });

    //FILTER PRODUCT
    $('.modal .filter-product').on('click', function(e){
        e.preventDefault();
        var form_id = $(this).closest('form').attr('id');
        var url = $(this).closest('form').attr('action');
        var form = $(this).closest('form').serialize();
        $.post(url, form,function(data,status,xhr){},'json')
            .done(function(response){
                $('#' + form_id + ' .modal.form-group').removeClass('has-error').find('.help-block').text('');
                if(response.status == 'ok'){
                    console.log(response.model);
                    $('.modal').modal('hide');
                    $('.product-list[data-type-id="' + response.type_id + '"]').replaceWith(response.html);
                }
                else if(response.status == 'get-errors'){
                    for(var i in response.errors){
                        $('#' + form_id + ' .form-group.field-productfilter-'+i).addClass('has-error').find('.help-block').text(response.errors[i]);
                    }
                }
                else {
                    alert(response.message);
                }
            })
            .fail();
    });
});