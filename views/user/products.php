<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Helper;
?>
<div class="site">
    <div class="col-xs-12" >
        <?php if(Yii::$app->session->getFlash('profilePageClass') and Yii::$app->session->getFlash('profilePageMessage')):?>
            <?php echo
            \yii\bootstrap\Alert::widget([
                    'options' =>
                        ['class' => Yii::$app->session->getFlash('profilePageClass') ],
                    'body' => Yii::$app->session->getFlash('profilePageMessage')
                ]
            )
            ?>
        <?php endif;?>
    </div>

    <div class="col-xs-12 custom-title">
        <?php if(Yii::$app->user->isGuest or
            (!is_null(Yii::$app->request->get('id')) and (Yii::$app->request->get('id') != Yii::$app->user->identity->getId()))):?>
            <h1><?= Yii::t('app', 'User`s advertisements');?></h1>
        <?php else:?>
            <h1><?= Yii::t('app', 'My advertisements');?></h1>
        <?php endif;?>
    </div>
    <div class="col-xs-12">
        <?php if(!Yii::$app->user->isGuest and is_null(Yii::$app->request->get('id'))):?>
            <?=Html::a(Yii::t('app', 'Create advertisement'), ['/product/create-product'], ['class' => 'btn custom-btn green col-xs-3']);?>
        <?php endif;?>
    </div>

    <?php if(count($products)):?>
        <div class="col-xs-12">
            <ul class="product-list">
                <?php foreach($products as $product): ?>
                    <li class="col-xs-12 product-tile">
                        <div class="col-xs-4 col-sm-4 col-md-2 picture">
                            <a href="<?= Url::to(['product/view-product', 'id' => $product->id]);?>">
                                <?= Helper::getImage($product->image_small_path, 'product');?>
                            </a>
                            <div class="owner-photo">
                                <a href="<?= Url::to(['user/profile', 'id' => $product->owner->id]);?>">
                                    <?= Helper::getImage($product->owner->image_small_path, 'user');?>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <div class="title"><?=$product->title;?></div>
                            <div class="description"><?=$product->description;?></div>
                            <div class="region">
                                <span><?= Yii::t('app', 'Region');?> : </span>
                                <?= Helper::validateStringField($product->region->name);?>
                            </div>
                            <div class="created">
                                <span><?= Yii::t('app', 'Time from create');?> : </span>
                                <?= Yii::$app->formatter->asDuration((time() - $product->created_at));?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <?=Html::a(Yii::t('app', 'View'), ['/product/view-product', 'id' => $product->id], ['class' => 'btn custom-btn green col-xs-6 col-sm-6 col-md-12']);?>
                            <?php if(!Yii::$app->user->isGuest and ($product->owner->id == Yii::$app->user->identity->getId())):?>
                                <?=Html::a(Yii::t('app', 'Edit'), ['/product/edit-product', 'id' => $product->id], ['class' => 'btn custom-btn blue col-xs-6 col-sm-6 col-md-12']);?>
                            <?php endif;?>
                        </div>
                    </li>
                <?php endforeach ;?>
            </ul>
        </div>
    <?php endif;?>
</div>