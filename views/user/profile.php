<?php
use yii\helpers\Html;
use app\models\Helper;
?>
<div class="row user">
    <div class="col-xs-12">
        <?php if(Yii::$app->session->getFlash('profilePageClass') and Yii::$app->session->getFlash('profilePageMessage')):?>
            <?php echo
            \yii\bootstrap\Alert::widget([
                    'options' =>
                        ['class' => Yii::$app->session->getFlash('profilePageClass') ],
                    'body' => Yii::$app->session->getFlash('profilePageMessage')
                ]
            )
            ?>
        <?php endif;?>
    </div>

    <div class="col-xs-12 page-title">
        <h1><?= Yii::t('app', 'User profile');?></h1>
    </div>

    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Avatar');?> : </div>
        <div class="col-sm-9 col-xs-12">
            <?php if(!Yii::$app->user->isGuest and ($user->id == Yii::$app->user->identity->getId())):?>
                <?= Html::a(Yii::t('app', 'My advertisements'), ['/user/products'], ['class' => 'btn custom-btn blue col-xs-4 pull-right link-products']) ?>
                <?= Helper::getImage($user->image_small_path, 'user');?>
            <?php else:?>
                <?= Html::a(Yii::t('app', 'User`s advertisements'), ['/user/products', 'id' => $user->id], ['class' => 'btn custom-btn green col-xs-4 pull-right link-products']) ?>
                <?= Helper::getImage($user->image_small_path, 'user');?>
            <?php endif;?>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Email');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->email;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Username');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->username;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Last name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->last_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'First name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->first_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Second name');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->second_name;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Birthday');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->birthday;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'About myself');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->description;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?=Yii::t('app', 'Participant_visits');?>:</div>
        <div class="col-sm-9 col-xs-12"><?php if($user->participant_visits_checkbox == 0) echo '<input type="checkbox" disabled="">';?></div>
        <div class="col-sm-9 col-xs-12"><?php if($user->participant_visits_checkbox == 1) echo '<input type="checkbox" disabled="" checked="">';?></div>

    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?=Yii::t('app', 'Participant_competition');?>:</div>
        <div class="col-sm-9 col-xs-12"><?php if($user->participant_competition_checkbox == 0) echo '<input type="checkbox" disabled="">';?></div>
        <div class="col-sm-9 col-xs-12"><?php if($user->participant_competition_checkbox == 1) echo '<input type="checkbox" disabled="" checked="">';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?=Yii::t('app', 'License year');?>:</div>
        <div class="col-sm-9 col-xs-12"><?php if($user->license_checkbox == 0) echo '<input type="checkbox" disabled="">';?></div>
        <div class="col-sm-9 col-xs-12"><?php if($user->license_checkbox == 1) echo '<input type="checkbox" disabled="" checked="">';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?=Yii::t('app', 'Insurance');?>:</div>
        <div class="col-sm-9 col-xs-12"><?php if($user->insurance_checkbox == 0) echo '<input type="checkbox" disabled="">';?></div>
        <div class="col-sm-9 col-xs-12"><?php if($user->insurance_checkbox == 1) echo '<input type="checkbox" disabled="" checked="">';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Region');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=(isset($user->region->name)) ? $user->region->name : '';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'City');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->city;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Parapro level');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=(isset($user->parapro->name)) ? $user->parapro->name : '';?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Fly since');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->fly_since;?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-3 col-xs-12"><?= Yii::t('app', 'Fly hours amount');?> : </div>
        <div class="col-sm-9 col-xs-12"><?=$user->fly_hours_amount;?></div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <?php if(!Yii::$app->user->isGuest and ($user->id == Yii::$app->user->identity->getId())):?>
                <?=Html::a(Yii::t('app', 'Edit'), ['/user/edit'], ['class' => 'btn custom-btn green col-xs-3']);?>
                <?=Html::a(Yii::t('app', 'Claim to be a Pilot'), ['/pilot/add-pilot'], ['class' => 'btn custom-btn blue col-xs-offset-1 col-xs-3']);?>
                <?=Html::a(Yii::t('app', 'Back'), ['/'], ['class' => 'btn custom-btn default col-xs-offset-1 col-xs-3']);?>
            <?php else:?>
                <?=Html::a(Yii::t('app', 'Back'), ['/'], ['class' => 'btn custom-btn default col-xs-6']);?>
            <?php endif;?>
        </div>
    </div>
</div>