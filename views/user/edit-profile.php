<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use cyneek\yii2\widget\upload\crop\UploadCrop;
?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-profile',
    'action' => Url::to(['/user/edit']),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]); ?>

    <div class="col-xs-12 form-title">
        <h1><?= Yii::t('app', 'Editing profile');?></h1>
    </div>

    <div class="col-xs-12">
        <div class="form-group field-userform-name has-success">
            <label class="col-sm-3 control-label" for="userform-name"><?=Yii::t('app', 'Current user image');?></label>
            <div class="col-sm-6">
                <?php if($model->image_small_path and isset($model->image_small_path)):?>
                    <?=Html::img($model->image_small_path, ['class' => 'img-responsive user-avatar-profile']);?>
                <?php else:?>
                    <?=Html::img(Yii::$app->params['empty_user_photo'], ['class' => 'img-responsive user-avatar-profile']);?>
                <?php endif;?>
            </div>
        </div>
        <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
            'jcropOptions' => [
                'aspectRatio' => 1,
                'rotatable' => false,
                'minContainerWidth' => 200,
                'minContainerHeight' => 200
            ]
        ]); ?>
    </div>

<?= $form->field($model, 'email')->input('text', ['readonly' => 'true']) ?>
<?= $form->field($model, 'username')->input('text', ['readonly' => 'true'])?>
<?= $form->field($model, 'last_name') ?>
<?= $form->field($model, 'first_name') ?>
<?= $form->field($model, 'second_name') ?>
<?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
    'options' => [
        'placeholder' => Yii::t('app', 'Select birthday'),
    ],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true
    ]])?>
<?= $form->field($model, 'region_id')->dropDownList($dropdownRegions,  ['prompt' => Yii::t('app','Choose region')]);?>
<?= $form->field($model, 'city')?>
<?= $form->field($model, 'phone',
    ['template' =>
        "{label}\n<div class=\"col-sm-6\">
            <div class=\"input-group\"><div class=\"input-group-addon\">+380</div>{input}</div>
            </div>\n<div class=\"col-xs-6\">{error}</div>"
    ])->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '99-9-999-999',
]) ?>
<?= $form->field($model, 'description')->textarea(['rows' => '3', 'maxlength' => 250]);?>
<div class=" col-xs-offset-3">
    <div class="col-xs-4"><?= $form->field($model, 'participant_visits_checkbox')->checkbox();?>
    </div>
    <div class="col-xs-5"><?= $form->field($model, 'participant_competition_checkbox')->checkbox();?></div>
</div>
<?= $form->field($model, 'ukraine_license_number')?>
<?= $form->field($model, 'parapro_id')->dropDownList($levelsList,  ['prompt' => Yii::t('app','Choose level')]);?>
<?= $form->field($model, 'fai_license_number')?>
<div class=" col-xs-offset-3">
    <div class="col-xs-4"><?= $form->field($model, 'license_checkbox')->checkbox();?></div>
    <div class="col-xs-5"><?= $form->field($model, 'insurance_checkbox')->checkbox();?></div>
</div>
<?= $form->field($model, 'fly_since')->widget(DatePicker::classname(), [
    'options' => [
        'placeholder' => Yii::t('app', 'Select date'),
    ],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true
    ]])?>
<?= $form->field($model, 'fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
<?= $form->field($model, 'average_fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
<?= $form->field($model, 'wing')?>
<?= $form->field($model, 'reserve_parachute')?>
<?= $form->field($model, 'helmet')?>
<?= $form->field($model, 'devices')?>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn custom-btn green col-sm-6', 'name' => 'edit-profile-button']);?>

            <?= Html::a(Yii::t('app', 'Back'), ['/user/profile'], ['class' => 'btn custom-btn default col-xs-4 pull-right']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>