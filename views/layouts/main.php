<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Paraplan',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'id' => 'custom-header'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => Yii::t('app', 'Federation'), 'url' => ['/']],
            ['label' => Yii::t('app', 'Clubs'), 'url' => ['/club/index']],
            ['label' => Yii::t('app', 'Contests'), 'url' => ['/']],
            ['label' => Yii::t('app', 'Schools'), 'url' => ['/']],
            ['label' => Yii::t('app', 'Fly`s points'), 'url' => ['/']],
            ['label' => Yii::t('app', 'Forum'), 'url' => ['/']],
            ['label' => Yii::t('app', 'Callboard'), 'url' => ['/product/index']]
        ],
    ]);
    if(Yii::$app->user->isGuest){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                \kartik\date\DatePicker::widget([
                            'name' => '',
                            'type' => \kartik\date\DatePicker::TYPE_BUTTON,
                            'value' => date('d-M-Y', strtotime('today')),
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'autoclose' => true,
                                'todayHighlight' => true
                            ],
                            'options' => [
                                'id' => 'main-calendar'
                            ]
                            ]),
                ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']],
                ['label' => Yii::t('app', 'Sign up'), 'url' => ['/site/sign-up']]
            ],
        ]);
        NavBar::end();
    }
    else{
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                \kartik\date\DatePicker::widget([
                    'name' => '',
                    'type' => \kartik\date\DatePicker::TYPE_BUTTON,
                    'value' => date('d-M-Y', strtotime('today')),
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        'todayHighlight' => true
                    ],
                    'options' => [
                        'id' => 'main-calendar'
                    ]
                ]),
                ['label' =>   Yii::t('app', 'Welcome').' , '.Yii::$app->user->identity->username,
                        'items' => [
                            ['label' => Yii::t('app','Profile'), 'url' => ['/user/profile']],
                            (Yii::$app->user->identity->role_id == 4)?
                                ['label' => Yii::t('app','Admin panel'), 'url' => ['/admin/board/users']]:'',
                                ['label' => Yii::t('app','Claim to be a Pilot'), 'url' => ['/pilot/add-pilot']],

                            ['label' => Yii::t('app','Logout'),
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                            ],
                        ]
                ]
            ],
        ]);
        NavBar::end();
    }

    ?>

    <?php if((Yii::$app->controller->id == 'site') and (Yii::$app->controller->action->id == 'index')):?>
        <section class="main-bg-image">
            <div class="container">
                <div class="row">
                        <div class="col-xs-offset-6">


                            <section class="slider-index">
                                <div id="carousel-index" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-index" data-slide-to="0" class="active"></li>
                                    </ol>

                                    <!-- Wrapper for slides -->

                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <img src="/images/first.jpg" class="col-xs-12 image" alt="...">
                                        </div>
                                    </div>
<!--
                                    </div>
                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-index" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-index" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </section>


                        </div>
                </div>
            </div>

        </section>
        <div class="container site index">
            <?= $content ?>
        </div>

    <?php else :?>
        <div class="container site">
            <?= $content ?>
        </div>
    <?php endif;?>
</div>

<footer class="footer">
    <?php
    NavBar::begin([
        'brandLabel' => 'Paraplan',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-bottom',
            'id' => 'custom-footer'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => Yii::t('app', 'Federation'), 'url' => ['#']],
            ['label' => Yii::t('app', 'Contests'), 'url' => ['#']],
            ['label' => Yii::t('app', 'Schools'), 'url' => ['#']],
            ['label' => Yii::t('app', 'Fly`s points'), 'url' => ['#']],
            ['label' => Yii::t('app', 'Forum'), 'url' => ['#']],
            ['label' => Yii::t('app', 'Callboard'), 'url' => ['/product/index']]
        ]]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app', 'F'), 'url' => ['#']],
            ['label' => Yii::t('app', 'V'), 'url' => ['#']],
            ['label' => Yii::t('app', 'T'), 'url' => ['#']],
            ['label' => Yii::t('app', 'G+'), 'url' => ['#']],
        ]]);
    NavBar::end();

    ?>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
