<?php
$this->title = 'Создание пилота';
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
?>

<?php $form = ActiveForm::begin([
    'id' => 'add-pilot',
    'action' => Url::toRoute(['/pilot/add-pilot']),
    'options' => [
        'class' => 'form-horizontal admin',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]);?>

    <div class="col-xs-12 form-title">
        <h1><?= Yii::t('app', 'Claim to be a Pilot');?></h1>
    </div>

<?= $form->field($model, 'last_name') ?>
<?= $form->field($model, 'first_name') ?>
<?= $form->field($model, 'second_name') ?>
<?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
    'options' => [
        'placeholder' => Yii::t('app', 'Select birthday'),
    ],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true
    ]])?>
<?= $form->field($model, 'region_id')->dropDownList($dropdownRegions,  ['prompt' => Yii::t('app','Choose region')]);?>
<?= $form->field($model, 'city')?>
<?= $form->field($model, 'phone',
    ['template' =>
        "{label}\n<div class=\"col-sm-6\">
            <div class=\"input-group\"><div class=\"input-group-addon\">+380</div>{input}</div>
            </div>\n<div class=\"col-xs-6\">{error}</div>"
    ])->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '99-9-999-999',
]) ?>
<?= $form->field($model, 'description')->textarea(['rows' => '3', 'maxlength' => 250]);?>
    <div class=" col-xs-offset-3">
        <div class="col-xs-12"><?= $form->field($model, 'participant_visits_checkbox')->checkbox();?></div>
        <div class="col-xs-12"><?= $form->field($model, 'participant_competition_checkbox')->checkbox();?></div>
    </div>
<?= $form->field($model, 'ukraine_license_number')?>
<?= $form->field($model, 'parapro_id')->dropDownList($levelsList,  ['prompt' => Yii::t('app','Choose level')]);?>
<?= $form->field($model, 'fai_license_number')?>
    <div class=" col-xs-offset-3">
        <div class="col-xs-12"><?= $form->field($model, 'license_checkbox')->checkbox();?></div>
        <div class="col-xs-12"><?= $form->field($model, 'insurance_checkbox')->checkbox();?></div>
    </div>
<?= $form->field($model, 'fly_since')->widget(DatePicker::classname(), [
    'options' => [
        'placeholder' => Yii::t('app', 'Select date'),
    ],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true
    ]])?>
<?= $form->field($model, 'fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
<?= $form->field($model, 'average_fly_hours_amount')->input('number', ['min' => 0, 'max' => 9999999, 'step' => 1])?>
<?= $form->field($model, 'wing')?>
<?= $form->field($model, 'reserve_parachute')?>
<?= $form->field($model, 'helmet')?>
<?= $form->field($model, 'devices')?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?=Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn custom-btn green col-xs-4 save-pilot', 'id' => 'add-pilot']);?>

            <?=Html::a(Yii::t('app', 'Back'), ['/user/profile'], ['class' => 'btn custom-btn default col-xs-4 pull-right back-pilot']);?>
        </div>
    </div>

<?php $form = ActiveForm::end(); ?>