<?php
$this->title = Yii::t('app', 'View advertisement') . ' : ' . $product->title;
use app\models\Helper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\captcha\Captcha;
use yii\bootstrap\Html;
?>
<div class="col-xs-offset-3 form-title">
    <h1><?= $this->params['breadcrumbs'][] = $this->title;?></h1>
</div>
<hr>

<div class="row view-product">

    <div class="col-xs-12 col-sm-6 col-md-4 image">
        <?= Helper::getImage($product->image_small_path, 'product');?>
        <span class="glyphicon glyphicon-eye-open">
            <span class="counter">
                <?= $product->show_counter;?>
            </span>
        </span>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8 main-info">


        <div class="title col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Advertisement name');?> : </span>
            <span class="value col-md-8"><?= $product->title;?></span>
        </div>
        <div class="description col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Advertisement description');?> : </span>
            <span class="value col-md-8"><?= $product->description;?></span>
        </div>
        <div class="owner col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Owner');?> : </span>
            <span class="value col-md-8"><?= $product->owner->username;?></span>
        </div>
        <div class="price col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Advertisement Price');?> : </span>
            <span class="value col-md-8 glyphicon-euro"><?= $product->price;?></span>
        </div>
        <div class="created_at col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Created at');?> : </span>
            <span class="value col-md-8"><?= date('d.m.Y', $product->created_at);?></span>
        </div>
        <div class="col-xs-12">
            <span class="key col-xs-12 col-md-4"><?= Yii::t('app', 'Region');?>:</span>
            <span class="value col-md-8"><?= $product->region->name;?></span>
        </div>
    </div>

    <?php if(count($product->photosFromGallery)):?>
        <div class="col-xs-12 gallery">

        </div>
    <?php endif;?>
    <div class="col-xs-12">

        <?php $form = ActiveForm::begin([
            'id' => 'edit-product',
            'action' => Url::toRoute(['/comment/to-product']),
            'options' => [
                'class' => 'form-horizontal',
            ],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-9\">{error}</div>",
                'labelOptions' => ['class' => 'col-sm-3 control-label'],
            ],
        ]); ?>

        <?= $form->field($comment, 'subject');?>

        <?= $form->field($comment, 'body')->textArea(['rows' => 6]);?>

        <?= $form->field($comment, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary col-xs-4 pull-left', 'name' => 'contact-button']) ?>
            <?= Html::a(Yii::t('app', 'Back'), ['/user/products'], ['class' => 'btn btn-default col-xs-4 pull-right']) ?>
        </div>


        <?php ActiveForm::end();?>
    </div>
</div>
