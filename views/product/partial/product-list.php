<?php
use app\models\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if(count($products)):?>
    <ul class="product-list col-xs-12" data-type-id="<?=$type_id;?>">
                    <?php foreach($products as $product): ?>
        <li class="col-xs-12 product-tile">
            <div class="col-xs-4 col-sm-4 col-md-2 picture">
                <a href="<?= Url::to(['product/view-product', 'id' => $product->id]);?>">
                    <?= Helper::getImage($product->image_small_path, 'product');?>
                    <span class="glyphicon glyphicon-eye-open">
                        <span class="counter">
                            <?= $product->show_counter;?>
                        </span>
                    </span>
                </a>
                <div class="owner-photo">
                    <a href="<?= Url::to(['user/profile', 'id' => $product->owner->id]);?>">
                        <?= Helper::getImage($product->owner->image_small_path, 'user');?>
                    </a>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="title"><?=$product->title;?></div>
                <div class="description"><?=$product->description;?></div>
                <div class="region">
                    <span><?= Yii::t('app', 'Region');?> : </span>
                    <?= Helper::validateStringField($product->region->name);?>
                </div>
                <div class="created">
                    <span><?= Yii::t('app', 'Time from create');?> : </span>
                    <?= Yii::$app->formatter->asDuration((time() - $product->created_at));?>
                </div>
            </div>
            <div class="col-xs-12 col-md-2">
                <?=Html::a(Yii::t('app', 'More'), ['/product/view-product', 'id' => $product->id], ['class' => 'btn custom-btn green col-xs-6 col-sm-6 col-md-12']);?>
                <?php if(!Yii::$app->user->isGuest and ($product->owner->id == Yii::$app->user->identity->getId())):?>
                    <?=Html::a(Yii::t('app', 'Edit'), ['/product/edit-product', 'id' => $product->id], ['class' => 'btn custom-btn blue col-xs-6 col-sm-6 col-md-12']);?>
                <?php endif;?>
            </div>
        </li>
    <?php endforeach ;?>
</ul>

<?php else:?>
    <ul class="product-list col-xs-12" data-type-id="<?=$type_id;?>">
        <?= Yii::t('app-error', 'Data was not found');?>
    </ul>
<?php endif;?>