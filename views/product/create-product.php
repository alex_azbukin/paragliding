<?php
$this->title = Yii::t('app', 'Product creating');
//use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use cyneek\yii2\widget\upload\crop\UploadCrop;
use kartik\file\FileInput;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\color\ColorInput;
?>


<?php $form = ActiveForm::begin([
    'id' => 'create-product',
    'action' => Url::toRoute(['/product/create-product']),
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
    ],
]);?>

<div class="col-xs-12 form-title">
    <h1><?= Yii::t('app', 'Advertisement creating');?></h1>
</div>

<div class="col-xs-12">
    <?= UploadCrop::widget(['form' => $form, 'model' => $model, 'attribute' => 'image',
        'jcropOptions' => [
            'aspectRatio' => 1,
            'rotatable' => false,
            'minContainerWidth' => 200,
            'minContainerHeight' => 200
        ]
    ]); ?>
</div>


<?= $form->field($model, 'title')->textInput();?>
<?= $form->field($model, 'description')->textarea();?>
<?= $form->field($model, 'region_id')->dropDownList($destinationDropdown, ['prompt' => Yii::t('app','Choose region')]);?>

<?= $form->field($model, 'galleryPhotos[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => Yii::$app->params['max_upload_photos'],
            'showRemove' => true,
            'showUpload' => false
        ]
]);?>

<?= $form->field($model, 'manufacturer');?>

<?= $form->field($model, 'manufacturer_url');?>

<?= $form->field($model, 'model');?>

<?= $form->field($model, 'size_id')->dropDownList($sizesList, ['prompt' => Yii::t('app','Choose size')]);?>

<?= $form->field($model, 'year_of_issue')->dropDownList(\app\models\Helper::yearList());?>

<?= $form->field($model, 'state')->dropDownList([
    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5
]);?>

<?= $form->field($model, 'defect');?>

<?= $form->field($model, 'sale_reason');?>

<?= $form->field($model, 'price', ['template' => "{label}\n<div class=\"col-sm-6 input-group price-group\">{input}<span class=\" input-group-addon  glyphicon-euro\"></span></div>\n<div class=\"col-sm-6\">{error}</div>"])->input('number', ['min' => 0, 'value' => 0]);?>

<?= $form->field($model, 'type_id')->dropDownList([
    0 => Yii::t('app', 'Another'),
    1 => Yii::t('app', 'Wing'),
    2 => Yii::t('app', 'Harness'),
    3 => Yii::t('app', 'Reserve parachute')
]);?>
<div class="advertisement attributes-1 hidden">

    <?= FieldRange::widget([
    'form' => $form,
    'model' => $model,
    'label' => Yii::t('app', 'Min-max weight'),
    'attribute1' => 'min_weight',
    'attribute2' => 'max_weight',
    'type' => FieldRange::INPUT_TEXT,
    'separator' => '----',
    'labelOptions' => ['class' => 'col-sm-3 control-label'],
    'template' => "{label}\n<div class=\"col-sm-6\">{widget}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>"
    ]);?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
    'options' => ['placeholder' => 'Select color'],
    ]);?>

    <?= $form->field($model, 'class_id')->dropDownList($classesList, ['prompt' => Yii::t('app','Choose class')]);?>

    <?= $form->field($model, 'raid', ['template' => "{label}\n<div class=\"col-sm-6 input-group price-group\">{input}<span class=\"input-group-addon\">" .Yii::t('app', 'hours') ."</span></div>\n<div class=\"col-sm-6\">{error}</div>"])->input('number', ['min' => 0, 'value' => 0]);?>
</div>

<div class="advertisement attributes-2 hidden">
    <?= FieldRange::widget([
        'form' => $form,
        'model' => $model,
        'label' => Yii::t('app', 'Min-max growth'),
        'attribute1' => 'min_growth',
        'attribute2' => 'max_growth',
        'type' => FieldRange::INPUT_TEXT,
        'separator' => '----',
        'labelOptions' => ['class' => 'col-sm-3 control-label'],
        'template' => "{label}\n<div class=\"col-sm-6\">{widget}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>"
    ]);?>

    <?= $form->field($model, 'suspension_id')->dropDownList($suspensionList, ['prompt' => Yii::t('app','Choose suspension type')]);?>

    <?= $form->field($model, 'tread_id')->dropDownList($treadsList, ['prompt' => Yii::t('app','Choose tread type')]);?>

    <?= $form->field($model, 'equipment_id')->dropDownList($equipmentList, ['prompt' => Yii::t('app','Choose equipment type')]);?>

    <?= $form->field($model, 'raid', ['template' => "{label}\n<div class=\"col-sm-6 input-group price-group\">{input}<span class=\"input-group-addon\">" .Yii::t('app', 'hours') ."</span></div>\n<div class=\"col-sm-6\">{error}</div>"])->input('number', ['min' => 0, 'value' => 0]);?>
</div>

<div class="advertisement attributes-3 hidden">
    <?= $form->field($model, 'count_of_use', ['template' => "{label}\n<div class=\"col-sm-6 input-group price-group\">{input}<span class=\"input-group-addon\">" .Yii::t('app', 'jumps') ."</span></div>\n<div class=\"col-sm-6\">{error}</div>"])->input('number', ['min' => 0, 'value' => 0]);?>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-6">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn custom-btn green col-xs-4 save-product', 'id' => 'create-product']);?>
        <?=Html::a('Назад', ['/user/products'], ['class' => 'btn custom-btn default col-xs-4 pull-right back-product']);?>
    </div>
</div>



<?php $form = ActiveForm::end() ?>
