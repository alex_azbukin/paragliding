<?php

use yii\helpers\Html;
use app\models\Helper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\slider\Slider;

$this->title = Yii::t('app',  'Advertisements');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index">

    <div class="col-xs-12 custom-title">
        <h1><?= Yii::t('app',  'Advertisements');?></h1>
    </div>

<div>
    <?=Html::a(Yii::t('app', 'Create advertisement'), ['/product/create-product'], ['class' => 'btn custom-btn green col-xs-6 col-sm-4 col-md-3 pull-right']);?>
</div>
<?php if(count($products)):?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs col-xs-12 advertisements" role="tablist">
        <li role="presentation" class="active">
            <a href="#wings" class="tab-advertisement wings" aria-controls="wings" role="tab" data-toggle="tab"><?=Yii::t('app', 'Wings');?></a>
        </li>
        <li role="presentation">
            <a href="#harness" class="tab-advertisement harness" aria-controls="harness" role="tab" data-toggle="tab"><?=Yii::t('app', 'Harness');?></a>
        </li>
        <li role="presentation">
            <a href="#reserve_parachute" class="tab-advertisement reserve_parachute" aria-controls="reserve_parachute" role="tab" data-toggle="tab"><?=Yii::t('app', 'Reserve parachute');?></a>
        </li>
        <li role="presentation">
            <a href="#another" class="tab-advertisement another" aria-controls="another" role="tab" data-toggle="tab"><?=Yii::t('app', 'Another');?></a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content col-xs-12">
        <div role="tabpanel" class="tab-pane fade active in" id="wings">
            <button type="button" class="btn custom-btn yellow col-xs-3" data-toggle="modal" data-target="#wingsFilter">
                <?= Yii::t('app', 'Filter');?>
            </button>
            <?= $this->renderAjax('//product/partial/product-list', ['products' => $products['wings'], 'type_id' => 1]);?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="harness">
            <button type="button" class="btn custom-btn yellow col-xs-3" data-toggle="modal" data-target="#harnessFilter">
                <?= Yii::t('app', 'Filter');?>
            </button>
            <?= $this->renderAjax('//product/partial/product-list', ['products' => $products['harness'], 'type_id' => 2]);?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="reserve_parachute">
            <button type="button" class="btn custom-btn yellow col-xs-3" data-toggle="modal" data-target="#reserve_parachuteFilter">
                <?= Yii::t('app', 'Filter');?>
            </button>
            <?= $this->renderAjax('//product/partial/product-list', ['products' => $products['reserve_parachute'], 'type_id' => 3]);?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="another">
            <button type="button" class="btn custom-btn yellow col-xs-3" data-toggle="modal" data-target="#anotherFilter">
                <?= Yii::t('app', 'Filter');?>
            </button>
            <?= $this->renderAjax('//product/partial/product-list', ['products' => $products['another'], 'type_id' => 0]);?>
        </div>
    </div>
<?php endif;?>
    <div class="form-group">
        <?=Html::a(Yii::t('app', 'Back'), ['/'], ['class' => 'btn custom-btn default pull-right col-xs-3']);?>
    </div>
</div>
<!-- Modal Filter WING-->
<!-------------------------------------------------------- 1 ------------------------------------------------------------------------->

<div class="modal fade" id="wingsFilter" tabindex="-1" role="dialog" aria-labelledby="wingsFilterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Filter "Wings"');?></h4>
            </div>
            <div class="modal-body">
                <?php $formWing = ActiveForm::begin([
                    'id' => 'filter-product-wing',
                    'action' => Url::toRoute(['/product/filter-product']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    ],
                ]); ?>

                <?= $formWing->field($productFilter, 'title');?>

                <?= $formWing->field($productFilter, 'class_id')->dropDownList($classesList);?>

                <?= $formWing->field($productFilter, 'weight');?>

                <?= $formWing->field($productFilter, 'year_of_issue')->dropDownList(\app\models\Helper::yearList());?>

                <div class="form-group field-productfilter-price">
                    <label class="col-sm-3 control-label" for="productfilter-price"><?= Yii::t('app', 'Advertisement Price');?></label>
                    <div class="col-sm-6">
                        <?= Slider::widget([
                            'name'=>'ProductFilter[price]',
                            'value'=>7,
                            'sliderColor'=>Slider::TYPE_GREY,
                            'handleColor'=>Slider::TYPE_DANGER,
                            'pluginOptions'=>[
                                'range' => true
                            ],
                            'options' => [
                                'class' => 'form-control'
                            ]
                        ]);?>
                    </div>
                </div>

                <?= $formWing->field($productFilter, 'type_id')->hiddenInput(['value' => 1])->label('');?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="button" class="btn custom-btn default pull-left" data-dismiss="modal"><?= Yii::t('app', 'Close');?></button>

                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn custom-btn green col-xs-4 pull-right filter-product', 'name' => 'filter-product-wing-button']);?>
                    </div>
                </div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------------- 2 ----------------------------------------------------------------------->

<div class="modal fade" id="harnessFilter" tabindex="-2" role="dialog" aria-labelledby="harnessFilterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Filter "Harness"');?></h4>
            </div>
            <div class="modal-body">
                <?php $formHarness = ActiveForm::begin([
                    'id' => 'filter-product-harness',
                    'action' => Url::toRoute(['/product/filter-product']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    ],
                ]); ?>

                <?= $formHarness->field($productFilter, 'title');?>

                <?= $formHarness->field($productFilter, 'suspension_id')->dropDownList($suspensionList);?>

                <?= $formHarness->field($productFilter, 'tread_id')->dropDownList($treadsList);?>

                <?= $formHarness->field($productFilter, 'equipment_id')->dropDownList($equipmentList);?>

                <div class="form-group field-productfilter-price">
                    <label class="col-sm-3 control-label" for="productfilter-price"><?= Yii::t('app', 'Advertisement Price');?></label>
                    <div class="col-sm-6">
                        <?= Slider::widget([
                            'name'=>'ProductFilter[price]',
                            'value'=>7,
                            'sliderColor'=>Slider::TYPE_GREY,
                            'handleColor'=>Slider::TYPE_DANGER,
                            'pluginOptions'=>[
                                'range' => true
                            ],
                            'options' => [
                                'class' => 'form-control'
                            ]
                        ]);?>
                    </div>
                </div>

                <?= $formHarness->field($productFilter, 'type_id')->hiddenInput(['value' => 2])->label('');?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="button" class="btn custom-btn default pull-left" data-dismiss="modal"><?= Yii::t('app', 'Close');?></button>

                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn custom-btn green col-xs-4 pull-right filter-product', 'name' => 'filter-product-harness-button']);?>
                    </div>
                </div>
                <?php ActiveForm::end();?>
            </div>


        </div>
    </div>
</div>

<!-------------------------------------------------------------- 3 -------------------------------------------------------------------------->

<div class="modal fade" id="reserve_parachuteFilter" tabindex="-3" role="dialog" aria-labelledby="reserve_parachuteFilterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Filter "Reserve parachute"');?></h4>
            </div>
            <div class="modal-body">
                <?php $formReserveParachute = ActiveForm::begin([
                    'id' => 'filter-product-reserve_parachute',
                    'action' => Url::toRoute(['/product/filter-product']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    ],
                ]); ?>

                <?= $formReserveParachute->field($productFilter, 'title');?>

                <?= $formReserveParachute->field($productFilter, 'count_of_use');?>


                <div class="form-group field-productfilter-price">
                    <label class="col-sm-3 control-label" for="productfilter-price"><?= Yii::t('app', 'Advertisement Price');?></label>
                    <div class="col-sm-6">
                        <?= Slider::widget([
                            'name'=>'ProductFilter[price]',
                            'value'=>7,
                            'sliderColor'=>Slider::TYPE_GREY,
                            'handleColor'=>Slider::TYPE_DANGER,
                            'pluginOptions'=>[
                                'range' => true
                            ],
                            'options' => [
                                'class' => 'form-control'
                            ]
                        ]);?>
                    </div>
                </div>

                <?= $formReserveParachute->field($productFilter, 'type_id')->hiddenInput(['value' => 3])->label('');?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="button" class="btn custom-btn default pull-left" data-dismiss="modal"><?= Yii::t('app', 'Close');?></button>

                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn custom-btn green col-xs-4 pull-right filter-product', 'name' => 'filter-product-reserve_parachute-button']);?>
                    </div>
                </div>
                <?php ActiveForm::end();?>
            </div>


        </div>
    </div>
</div>

<div class="modal fade" id="anotherFilter" tabindex="-0" role="dialog" aria-labelledby="anotherFilterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Filter "Another"');?></h4>
            </div>
            <div class="modal-body">
                <?php $formAnother = ActiveForm::begin([
                    'id' => 'filter-product-another',
                    'action' => Url::toRoute(['/product/filter-product']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    ],
                ]); ?>

                <?= $formAnother->field($productFilter, 'title');?>

                <div class="form-group field-productfilter-price">
                    <label class="col-sm-3 control-label" for="productfilter-price"><?= Yii::t('app', 'Advertisement Price');?></label>
                    <div class="col-sm-6">
                        <?= Slider::widget([
                            'name'=>'ProductFilter[price]',
                            'value'=>7,
                            'sliderColor'=>Slider::TYPE_GREY,
                            'handleColor'=>Slider::TYPE_DANGER,
                            'pluginOptions'=>[
                                'range' => true
                            ],
                            'options' => [
                                'class' => 'form-control'
                            ]
                        ]);?>
                    </div>
                </div>

                <?= $formAnother->field($productFilter, 'type_id')->hiddenInput(['value' => 0])->label('');?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="button" class="btn custom-btn default pull-left" data-dismiss="modal"><?= Yii::t('app', 'Close');?></button>

                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn custom-btn green col-xs-4 pull-right filter-product', 'name' => 'filter-product-another-button']);?>
                    </div>
                </div>
                <?php ActiveForm::end();?>
            </div>


        </div>
    </div>
</div>