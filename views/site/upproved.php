<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-vertical'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-offset-3 col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-offset-3 col-sm-6 control-label'],
    ],
]); ?>

    <?=$form->field($model, 'upproved')->hiddenInput(['value' => Yii::$app->request->get('upproved')])->label('');?>

<?php ActiveForm::end(); ?>