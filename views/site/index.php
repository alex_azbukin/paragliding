<?php

/* @var $this yii\web\View */

$this->title = 'Paraplan';
?>
<div class="tabs-index">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?=Yii::t('app', 'Contests');?></a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?=Yii::t('app', 'News');?></a></li>
        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?=Yii::t('app', 'Pilot`s club');?></a></li>
        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><?=Yii::t('app', 'Callboard');?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/first.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    Paragliding is the recreational and competitive adventure sport of flying paragliders:
                    lightweight, free-flying, foot-launched glider aircraft with no rigid primary structure.[1]
                    The pilot sits in a harness suspended below a fabric wing comprising a large number of interconnected
                    baffled cells. Wing shape is maintained by the suspension lines, the pressure of air entering vents
                    in the front of the wing, and the aerodynamic forces of the air flowing over the outside.
                </div>
            </div>
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/third.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    In 1954, Walter Neumark predicted (in an article in Flight magazine) a time when a glider pilot
                    would be "able to launch himself by running over the edge of a cliff or down a slope ... whether on
                    a rock-climbing holiday in Skye or ski-ing in the Alps."[7]

                    In 1961, the French engineer Pierre Lemoigne produced improved parachute designs that led to
                    the Para-Commander. The PC had cutouts at the rear and sides that enabled it to be towed into
                    the air and steered – leading to parasailing/parascending.
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/second.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    Paragliding is related to the following activities: Hang gliding is a close cousin, and hang-glider
                    and paraglider launches are often found in proximity to one another.[2] Despite the considerable
                    difference in equipment, the two activities offer similar pleasures, and some pilots are involved
                    in both sports.
                </div>
            </div>
            <div class="col-xs-3">
                <img src="/images/third.jpg" class="img-responsive">
            </div>
            <div class="col-xs-9">
                In 1954, Walter Neumark predicted (in an article in Flight magazine) a time when a glider pilot
                would be "able to launch himself by running over the edge of a cliff or down a slope ... whether on
                a rock-climbing holiday in Skye or ski-ing in the Alps."[7]

                In 1961, the French engineer Pierre Lemoigne produced improved parachute designs that led to
                the Para-Commander. The PC had cutouts at the rear and sides that enabled it to be towed into
                the air and steered – leading to parasailing/parascending.
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/first.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    In 1954, Walter Neumark predicted (in an article in Flight magazine) a time when a glider pilot
                    would be "able to launch himself by running over the edge of a cliff or down a slope ... whether on
                    a rock-climbing holiday in Skye or ski-ing in the Alps."[7]

                    In 1961, the French engineer Pierre Lemoigne produced improved parachute designs that led to
                    the Para-Commander. The PC had cutouts at the rear and sides that enabled it to be towed into
                    the air and steered – leading to parasailing/parascending.
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="settings">
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/third.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    Paragliding is the recreational and competitive adventure sport of flying paragliders:
                    lightweight, free-flying, foot-launched glider aircraft with no rigid primary structure.[1]
                    The pilot sits in a harness suspended below a fabric wing comprising a large number of interconnected
                    baffled cells. Wing shape is maintained by the suspension lines, the pressure of air entering vents
                    in the front of the wing, and the aerodynamic forces of the air flowing over the outside.
                </div>
            </div>
            <div class="col-xs-12 panel-row">
                <div class="col-xs-3">
                    <img src="/images/first.jpg" class="img-responsive">
                </div>
                <div class="col-xs-9">
                    In 1954, Walter Neumark predicted (in an article in Flight magazine) a time when a glider pilot
                    would be "able to launch himself by running over the edge of a cliff or down a slope ... whether on
                    a rock-climbing holiday in Skye or ski-ing in the Alps."[7]

                    In 1961, the French engineer Pierre Lemoigne produced improved parachute designs that led to
                    the Para-Commander. The PC had cutouts at the rear and sides that enabled it to be towed into
                    the air and steered – leading to parasailing/parascending.
                </div>
            </div>
        </div>
    </div>

</div>

