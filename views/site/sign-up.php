<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Sign up');
?>
<div class="site-signup">
    <div class="col-sm-offset-3 col-sm-6">
        <h1><?= Html::encode($this->title) ?></h1>

        <p><?= Yii::t('app', 'Please fill out the following fields to sign up');?> : </p>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'sign-up-form',
        'options' => ['class' => 'form-vertical'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-offset-3 col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
            'labelOptions' => ['class' => 'col-sm-offset-3 col-sm-6 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username'); ?>
    <?= $form->field($model, 'password')->passwordInput(); ?>
    <?= $form->field($model, 'password_repeat')->passwordInput(); ?>
    <?= $form->field($model, 'email'); ?>



    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn custom-btn green col-sm-3', 'name' => 'signUp-button']) ?>
        </div>
    </div>


    </div>

    <?php ActiveForm::end(); ?>

</div>
