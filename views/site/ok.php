<?php
use app\components\AdminAlertWidget;
use yii\helpers\Html;
//use yii\bootstrap\modal;
use yii\bootstrap;
?>
<?php
echo "<h3>На вашу почту отправлена ссылка для подтверждения регистрации</h3>";
?>


<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

<!--                <button class="close" type="button" data-dismiss="modal">×</button>-->
<!--                <h4 class="modal-title btn-danger">Предупреждение</h4>-->
            </div>
            <div class="modal-body"><h4>Извините, но у Вас нет прав. Нужно зарегестрироваться.</h4></div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button></div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-terget="<a href="/site/index/">Зарегестрироваться</button></div>
        </div>
    </div>
</div>
<br><br>
<div class="col-xs-12">
    <div class="col-xs-6">
        <?=Html::a('Login', ['/site/login'], ['class' => 'btn btn-primary col-xs-4 pull-left back-reset-password']);?>
    </div>
</div>

