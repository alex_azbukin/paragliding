<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-vertical'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-offset-3 col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-offset-3 col-sm-6 control-label'],
    ],
]); ?>

    <?= $form->field($model, 'password')->passwordInput();?>
    <?= $form->field($model, 'password_repeat')->passwordInput(); ?>
    <?=$form->field($model, 'token')->hiddenInput(['value' => Yii::$app->request->get('token')])->label('');?>

    <div class="col-xs-12">
        <div class="col-xs-offset-3 col-xs-6">
            <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success col-xs-4 save-pilot', 'id' => 'add-pilot']);?>

            <?=Html::a('Назад', ['/site/login'], ['class' => 'btn btn-default col-xs-4 pull-right back-reset-password']);?>
        </div>
    </div>

<?php ActiveForm::end(); ?>