<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
?>
<div class="site-login">
    <div class="col-sm-offset-3 col-sm-6">
        <?php if(Yii::$app->session->getFlash('loginPageClass') and Yii::$app->session->getFlash('loginPageMessage')):?>
            <?php echo
                \yii\bootstrap\Alert::widget([
                        'options' =>
                            ['class' => Yii::$app->session->getFlash('loginPageClass') ],
                        'body' => Yii::$app->session->getFlash('loginPageMessage')
                        ]
                    )
            ?>
        <?php endif;?>

        <h1><?= Html::encode($this->title) ?></h1>

        <p><?= Yii::t('app', 'Please fill out the following fields to login');?> : </p>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-vertical'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-offset-3 col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
            'labelOptions' => ['class' => 'col-sm-offset-3 col-sm-6 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-sm-offset-3 col-sm-6\">{input} {label}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn custom-btn green col-sm-6', 'name' => 'login-button']);?>

                <?= Html::a('Забыли пароль?', ['/site/reset-password'], ['class' => 'btn custom-btn blue col-xs-4 pull-right link-reset-password']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>


</div>
