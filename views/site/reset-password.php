<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-vertical'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-offset-3 col-sm-6\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-6\">{error}</div>",
        'labelOptions' => ['class' => 'col-sm-offset-3 col-sm-6 control-label'],
    ],
]); ?>

<?= $form->field($model, 'email');?>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-6">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary col-sm-6', 'name' => 'login-button']);?>
    </div>
</div>

<?php ActiveForm::end(); ?>
