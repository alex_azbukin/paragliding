<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<script type="text/javascript" id="map-api-script">
    function initialize() {
        var mapOptions = {
            zoom: window.MapZoomDefault,
            center: new google.maps.LatLng(window.MapLatitudeDefault,  window.MapLongitudeDefault)
        };

        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        <?php if(count($clubs)):?>
            <?php foreach($clubs as $key => $club):?>
                var marker_<?=$key;?> = new google.maps.Marker({
                    position: new google.maps.LatLng(<?=$club->latitude;?>,  <?=$club->longitude;?>),
                    map: map,
                    title: "<?= $club->name;?>",
                    draggable: true,
                    animation: google.maps.Animation.DROP
                });
                marker_<?=$key;?>.addListener('click', function() {
                    alert(marker_<?=$key;?>.title);
                });
            <?php endforeach;?>
        <?php endif;?>


    }

    function loadScript() {
        var script1 = document.createElement('script');
        script1.type = 'text/javascript';
        script1.id = 'map-api-script';
        script1.src = 'https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['GoogleMapAPIKey'];?>&callback=initialize';

        document.body.appendChild(script1);
    }

    loadScript();

</script>
<div class="col-sm-3 col-xs-12 pull-right search">
    <?php $form = ActiveForm::begin([
        'id' => 'club-list',
        'action' => Url::to(['/club/ajax-search']),
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-12\">{input}</div>\n<div class=\"col-sm-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-sm-12 control-label'],
        ],
    ]); ?>
    <?= $form->field($model, 'club_id')->dropDownList($clubsDropdown,  ['prompt' => Yii::t('app','Choose club')]);?>
    <?= $form->field($model, 'region_id')->dropDownList($regionDropdown,  ['prompt' => Yii::t('app','Choose region')]);?>

    <div class="form-group">
        <div class="col-xs-12">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn custom-btn blue col-sm-6', 'name' => 'search-button', 'id' => 'search-club']);?>
        </div>
    </div>
    <?php ActiveForm::end();?>
</div>
<div class="col-sm-9 col-xs-12">
    <div id="map-canvas" class="col-xs-12" style="height: 400px;"></div>
</div>
<div class="col-xs-12 search-result">

</div>
