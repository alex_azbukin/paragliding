<?php
return [
    'Current request is not allow' => 'Поточний запит не вирішено',
    'Clubs were not find. Please, change search condition' => 'Клуби не були знайдені. Будь ласка, змініть умови пошуку',
    'Advertisement were not find. Please, change search condition' => 'Оголошення не були знайдені. Будь ласка, змініть умови пошуку',
    'Data was not found' => 'Дані не були знайдені'
];