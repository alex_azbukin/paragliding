<?php
    return [
        'Current request is not allow' => 'Текущий запрос не разрешен',
        'Clubs were not find. Please, change search condition' => 'Клубы не были найдены. Пожалуйста, измените условия поиска',
        'Advertisement were not find. Please, change search condition' => 'Объявления не были найдены. Пожалуйста, измените условия поиска',
        'Data was not found' => 'Данные не были найдены'
    ];