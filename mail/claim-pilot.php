<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<br>
<p>
    <?= Yii::t('app', 'User has been submit claim to be a pilot');?> :

    <?=Html::a(Yii::t('app', 'User profile'), Url::to(['/admin/user/check-claim', 'id' => $user->id], [true]));?>
</p>