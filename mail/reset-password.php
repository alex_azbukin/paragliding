<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>

<p>
    <?= Yii::t('app', 'To change the password, you need to go to this link');?> :
    <?=Html::a(Yii::t('app', 'Restore password'), Url::to(['site/update-password', 'token' => $user->password_reset_token], [true]));?>

</p>

