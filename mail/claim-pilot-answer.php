<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<br>
<?php if($status == 'approved'):?>
<p>
    <?= Yii::t('app', 'Your claim was approved successful');?> :

    <?=Html::a(Yii::t('app', 'User profile'), Url::to(['/user/profile'], [true]));?>
</p>
<?php else:?>
<p>
    <?= Yii::t('app', 'Unfortunately, your claim was rejected');?> :

    <?=Html::a(Yii::t('app', 'User profile'), Url::to(['/user/profile'], [true]));?>
</p>
<?php endif;?>