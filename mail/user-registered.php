<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<br>
<p>
    <?= Yii::t('app', 'Congratulations, new user has been registered. Email');?> : <?= $user->email;?>
</p>