<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>

<p>
    <?= Yii::t('app', 'To confirm your registration click on the link');?> :
    <?=Html::a(Yii::t('app', 'Confirmation of registration'), Url::to(['approved', 'registration_token' => $user->registration_token], [true]));?>

</p>