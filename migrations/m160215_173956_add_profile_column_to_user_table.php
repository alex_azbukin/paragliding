<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_173956_add_profile_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'first_name',     $this->string(255));
        $this->addColumn('{{%user}}', 'second_name',    $this->string(255));
        $this->addColumn('{{%user}}', 'last_name',      $this->string(255));
        $this->addColumn('{{%user}}', 'birthday',      $this->date());
        $this->addColumn('{{%user}}', 'region_id',      $this->integer());
        $this->addColumn('{{%user}}', 'city',           $this->string(255));
        $this->addColumn('{{%user}}', 'phone', $this->string(255));
        $this->addColumn('{{%user}}', 'description', $this->string(255));
        $this->addColumn('{{%user}}', 'ukraine_license_number', $this->string(255));
        $this->addColumn('{{%user}}', 'parapro_id', $this->integer());
        $this->addColumn('{{%user}}', 'fai_license_number', $this->string(255));
        $this->addColumn('{{%user}}', 'fly_since', $this->dateTime());
        $this->addColumn('{{%user}}', 'fly_hours_amount', $this->integer());
        $this->addColumn('{{%user}}', 'average_fly_hours_amount', $this->integer());
        $this->addColumn('{{%user}}', 'wing', $this->string(255));
        $this->addColumn('{{%user}}', 'reserve_parachute', $this->string(255));
        $this->addColumn('{{%user}}', 'helmet', $this->string(255));
        $this->addColumn('{{%user}}', 'devices', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'second_name');
        $this->dropColumn('{{%user}}', 'last_name');
        $this->dropColumn('{{%user}}', 'birthday');
        $this->dropColumn('{{%user}}', 'region_id');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'phone');
        $this->dropColumn('{{%user}}', 'description');
        $this->dropColumn('{{%user}}', 'ukraine_license_number');
        $this->dropColumn('{{%user}}', 'parapro_id');
        $this->dropColumn('{{%user}}', 'fai_license_number');
        $this->dropColumn('{{%user}}', 'fly_since');
        $this->dropColumn('{{%user}}', 'fly_hours_amount');
        $this->dropColumn('{{%user}}', 'average_fly_hours_amount');
        $this->dropColumn('{{%user}}', 'wing');
        $this->dropColumn('{{%user}}', 'reserve_parachute');
        $this->dropColumn('{{%user}}', 'helmet');
        $this->dropColumn('{{%user}}', 'devices');

        echo "m160215_173956_add_profile_column_to_user_table was reverted successful.\n";
    }
}
