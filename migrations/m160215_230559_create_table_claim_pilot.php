<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_230559_create_table_claim_pilot extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%claim_pilot}}',[
            'id' => $this->primaryKey()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%claim_pilot}}');
        echo "m160215_230559_create_table_claim_pilot was reverted successful.\n";
        return false;
    }
}
