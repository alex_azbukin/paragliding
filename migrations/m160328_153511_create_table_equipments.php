<?php

use yii\db\Migration;

class m160328_153511_create_table_equipments extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%equipments}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
        $this->insert('{{%equipments}}', ['name' => 'Подножка']);
        $this->insert('{{%equipments}}', ['name' => 'Акселлератор']);
        $this->insert('{{%equipments}}', ['name' => 'Боковые протекторы']);
        $this->insert('{{%equipments}}', ['name' => 'Карбоновая сидушка']);
        $this->insert('{{%equipments}}', ['name' => 'Холдер для рации']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%equipments}}');
        echo "m160328_153511_create_table_equipments was reverted successful.\n";
    }
}
