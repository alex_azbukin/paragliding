<?php

use yii\db\Migration;

class m160328_152454_create_table_sizes extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%sizes}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255),
            'short_name' => $this->string(255),
            'description' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
        $this->insert('{{%sizes}}', ['name' => 'Small', 'short_name' => 'S']);
        $this->insert('{{%sizes}}', ['name' => 'Middle', 'short_name' => 'M']);
        $this->insert('{{%sizes}}', ['name' => 'Large', 'short_name' => 'L']);
        $this->insert('{{%sizes}}', ['name' => 'Extra Large', 'short_name' => 'XL']);
        $this->insert('{{%sizes}}', ['name' => 'Extra Extra Large', 'short_name' => 'XXL']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%sizes}}');
        echo "m160328_152454_create_table_sizes was reverted successful.\n";
    }
}
