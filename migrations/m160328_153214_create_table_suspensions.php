<?php

use yii\db\Migration;

class m160328_153214_create_table_suspensions extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%suspensions}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
        $this->insert('{{%suspensions}}', ['name' => 'Cocoon']);
        $this->insert('{{%suspensions}}', ['name' => 'Opened']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%suspensions}}');
        echo "m160328_153214_create_table_suspensions was reverted successful.\n";
    }
}
