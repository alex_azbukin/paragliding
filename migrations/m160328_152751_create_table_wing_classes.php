<?php

use yii\db\Migration;

class m160328_152751_create_table_wing_classes extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%wing_classes}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255),
            'short_name' => $this->string(255),
            'description' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);

        $this->insert('{{%wing_classes}}', ['short_name' => 'EN-A']);
        $this->insert('{{%wing_classes}}', ['short_name' => 'EN-B']);
        $this->insert('{{%wing_classes}}', ['short_name' => 'EN-C']);
        $this->insert('{{%wing_classes}}', ['short_name' => 'EN-D']);
        $this->insert('{{%wing_classes}}', ['short_name' => 'CCC']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%wing_classes}}');
        echo "m160328_152751_create_table_wing_classes was reverted successful.\n";
    }
}
