<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_065459_add_column_password_reset_token extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'password_reset_token', 'VARCHAR(255) AFTER `role_id`');
    }

    public function safeDown()
    {
        echo "m160210_065459_add_column_password_reset_token cannot be reverted.\n";

        $this->dropColumn('{{%user}}', 'password_reset_token', 'VARCHAR(255)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
