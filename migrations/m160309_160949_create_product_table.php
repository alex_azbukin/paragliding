<?php

use yii\db\Migration;

class m160309_160949_create_product_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'main_image' => $this->string(255)->notNull(),
            'region_id' => $this->integer(),
            'owner_id' => $this->integer(),
            'type_id' => $this->integer(),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);


    }


    public function safeDown()
    {
        echo " m160309_160949_create_product_table  was reverted successful.\n";
        $this->dropTable('{{%product}}');
    }
}
