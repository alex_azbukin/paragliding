<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_222808_change_default_value_approved_in_user extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'approved', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%user}}', 'approved', $this->smallInteger()->defaultValue(null));
        echo "m160218_222808_change_default_value_approved_in_user was reverted successful.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
