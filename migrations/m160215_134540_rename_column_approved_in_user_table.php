<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_134540_rename_column_approved_in_user_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%user}}', 'upproved', 'approved');

    }

    public function safeDown()
    {
        echo "m160215_134540_rename_column_approved_in_user_table was renamed successful.\n";

        $this->renameColumn('{{%user}}', 'approved', 'upproved');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
