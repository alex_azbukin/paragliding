<?php

use yii\db\Migration;

class m160315_101605_change_region_id_column_in_product_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%product}}', 'region_id', $this->string(255));
    }

    public function safeDown()
    {
        echo "m160315_101605_change_region_id_column_in_product_table cannot be reverted.\n";

        $this->alterColumn('{{%product}}', 'region_id', $this->integer(255));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
