<?php

use yii\db\Schema;
use yii\db\Migration;

class m160206_132338_create_table_clubs_admins extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%clubs_admins}}',[
            'club_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull()
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%clubs_admins}}');
        echo "m160206_132338_create_table_clubs_admins was reverted successful.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
