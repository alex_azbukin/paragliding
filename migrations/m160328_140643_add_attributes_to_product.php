<?php

use yii\db\Migration;

class m160328_140643_add_attributes_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'manufacturer',     $this->string(255));
        $this->addColumn('{{%product}}', 'model',     $this->string(255));
        $this->addColumn('{{%product}}', 'size_id',     $this->smallInteger());
        $this->addColumn('{{%product}}', 'min_weight',     $this->integer());
        $this->addColumn('{{%product}}', 'max_weight',     $this->integer());
        $this->addColumn('{{%product}}', 'color',     $this->string(50));
        $this->addColumn('{{%product}}', 'class_id',     $this->string(50));
        $this->addColumn('{{%product}}', 'year_of_issue',     $this->integer());
        $this->addColumn('{{%product}}', 'raid',     $this->integer());
        $this->addColumn('{{%product}}', 'state',     $this->smallInteger()->defaultValue(1));
        $this->addColumn('{{%product}}', 'defect',     $this->string(255));
        $this->addColumn('{{%product}}', 'sale_reason',     $this->string(255));
        $this->addColumn('{{%product}}', 'manufacturer_url',     $this->string(500));
        $this->addColumn('{{%product}}', 'is_new',     $this->smallInteger()->defaultValue(0));
        $this->addColumn('{{%product}}', 'price',     $this->integer());
        $this->addColumn('{{%product}}', 'min_growth',     $this->integer());
        $this->addColumn('{{%product}}', 'max_growth',     $this->integer());
        $this->addColumn('{{%product}}', 'suspension_id',     $this->string(255));
        $this->addColumn('{{%product}}', 'tread_id',     $this->string(255));
        $this->addColumn('{{%product}}', 'equipment_id',     $this->string(255));
        $this->addColumn('{{%product}}', 'count_of_use',     $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'manufacturer');
        $this->dropColumn('{{%product}}', 'model');
        $this->dropColumn('{{%product}}', 'size_id');
        $this->dropColumn('{{%product}}', 'min_weight');
        $this->dropColumn('{{%product}}', 'max_weight');
        $this->dropColumn('{{%product}}', 'color');
        $this->dropColumn('{{%product}}', 'class_id');
        $this->dropColumn('{{%product}}', 'year_of_issue');
        $this->dropColumn('{{%product}}', 'raid');
        $this->dropColumn('{{%product}}', 'state');
        $this->dropColumn('{{%product}}', 'defect');
        $this->dropColumn('{{%product}}', 'sale_reason');
        $this->dropColumn('{{%product}}', 'manufacturer_url');
        $this->dropColumn('{{%product}}', 'is_new');
        $this->dropColumn('{{%product}}', 'price');
        $this->dropColumn('{{%product}}', 'min_growth');
        $this->dropColumn('{{%product}}', 'max_growth');
        $this->dropColumn('{{%product}}', 'suspension_id');
        $this->dropColumn('{{%product}}', 'tread_id');
        $this->dropColumn('{{%product}}', 'equipment_id');
        $this->dropColumn('{{%product}}', 'count_of_use');

        echo "m160328_140643_add_attributes_to_product was reverted successful.\n";
    }
}
