<?php

use yii\db\Migration;

class m160221_185536_add_image_pathes_to_club_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%clubs}}', 'image_small_path', $this->string(255));
        $this->addColumn('{{%clubs}}', 'image_original_path', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%clubs}}', 'image_small_path');
        $this->dropColumn('{{%clubs}}', 'image_original_path');
        echo "m160221_185536_add_image_pathes_to_club_table was reverted successful.\n";
    }
}
