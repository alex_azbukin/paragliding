<?php

use yii\db\Migration;

class m160405_131019_add_counter_to_product_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'show_counter',     $this->integer()->defaultValue(0));
        $this->addColumn('{{%product}}', 'message_counter',     $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'show_counter');
        $this->dropColumn('{{%product}}', 'message_counter');
    }
}
