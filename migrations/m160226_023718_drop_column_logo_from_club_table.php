<?php

use yii\db\Migration;

class m160226_023718_drop_column_logo_from_club_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%clubs}}', 'photo');
        $this->dropColumn('{{%clubs}}', 'image_original_path');
    }

    public function safeDown()
    {
        $this->addColumn('{{%clubs}}', 'photo', $this->string(255));
        $this->addColumn('{{%clubs}}', 'image_original_path', $this->string(255));
        echo "m160226_023718_drop_column_logo_from_club_table was reverted successful.\n";
    }
}
