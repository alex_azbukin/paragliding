<?php

use yii\db\Migration;

class m160310_205319_add_image_small_path_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'image_small_path', $this->string(255));
    }

    public function safeDown()
    {
        echo "m160310_205319_add_image_small_path_column cannot be reverted.\n";

        $this->dropColumn('{{%product}}', 'image_small_path');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
