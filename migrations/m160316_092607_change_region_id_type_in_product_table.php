<?php

use yii\db\Migration;

class m160316_092607_change_region_id_type_in_product_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%product}}', 'region_id', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('{{%product}}', 'region_id', $this->string(255));
        echo "m160316_092607_change_region_id_type_in_product_table was reverted successful.\n";
    }
}
