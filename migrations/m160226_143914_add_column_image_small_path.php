<?php

use yii\db\Migration;

class m160226_143914_add_column_image_small_path extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'image_small_path', $this->string(255));
    }

    public function safeDown()
    {

        echo "m160226_143914_add_column_image_small_path cannot be reverted.\n";
        $this->dropColumn('{{%user}}', 'image_small_path');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
