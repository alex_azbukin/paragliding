<?php

use yii\db\Migration;

class m160311_135701_drop_column_main_image_from_product_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%product}}', 'main_image');
    }

    public function safeDown()
    {
        $this->addColumn('{{%product}}', 'main_image', $this->string(255));
        echo "m160311_135701_drop_column_main_image_from_product_table was reverted successful.\n";
    }
}
