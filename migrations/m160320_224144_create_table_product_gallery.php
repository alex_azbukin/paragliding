<?php

use yii\db\Migration;

class m160320_224144_create_table_product_gallery extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%product_gallery}}',[
            'id' => $this->primaryKey()->notNull(),
            'path' => $this->string(255),
            'product_id' => $this->integer(),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%product_gallery}}');
        echo "m160320_224144_create_table_product_gallery was reverted successful.\n";
    }
}
