<?php

use yii\db\Migration;

class m160224_174401_change_lat_long_for_clubs extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%clubs}}', 'latitude', $this->string(255));
        $this->alterColumn('{{%clubs}}', 'longitude', $this->string(255));

    }

    public function safeDown()
    {
        $this->alterColumn('{{%clubs}}', 'latitude', $this->integer());
        $this->alterColumn('{{%clubs}}', 'longitude', $this->integer());
        echo "m160224_174401_change_lat_long_for_clubs was reverted successful.\n";
    }
}
