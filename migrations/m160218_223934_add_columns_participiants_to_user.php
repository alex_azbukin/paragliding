<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_223934_add_columns_participiants_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'participant_visits_checkbox', $this->smallInteger(2)->defaultValue(0));
        $this->addColumn('{{%user}}', 'participant_competition_checkbox', $this->smallInteger(2)->defaultValue(0));
        $this->addColumn('{{%user}}', 'license_checkbox', $this->smallInteger(2)->defaultValue(0));
        $this->addColumn('{{%user}}', 'insurance_checkbox', $this->smallInteger(2)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'participant_visits_checkbox');
        $this->dropColumn('{{%user}}', 'participant_competition_checkbox');
        $this->dropColumn('{{%user}}', 'license_checkbox');
        $this->dropColumn('{{%user}}', 'insurance_checkbox');
        echo "m160218_223934_add_columns_participiants_to_user was reverted successful.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
