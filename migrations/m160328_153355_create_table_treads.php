<?php

use yii\db\Migration;

class m160328_153355_create_table_treads extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%treads}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
        $this->insert('{{%treads}}', ['name' => 'Муссбэг']);
        $this->insert('{{%treads}}', ['name' => 'Эирбэг']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%treads}}');
        echo "m160328_153355_create_table_treads was reverted successful.\n";
    }
}
