<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_213101_create_parapro_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%parapro}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);

        $this->insert('{{%parapro}}', ['name' => '--None--']);
        $this->insert('{{%parapro}}', ['name' => 'ParaPro(1)']);
        $this->insert('{{%parapro}}', ['name' => 'ParaPro(2)']);
        $this->insert('{{%parapro}}', ['name' => 'ParaPro(3)']);
        $this->insert('{{%parapro}}', ['name' => 'ParaPro(4)']);
        $this->insert('{{%parapro}}', ['name' => 'ParaPro(5)']);
        $this->insert('{{%parapro}}', ['name' => 'Instructor']);
        $this->insert('{{%parapro}}', ['name' => 'Tandem Pilot']);
        $this->insert('{{%parapro}}', ['name' => 'Expert']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%parapro}}');
        echo "m160215_213101_create_parapro_table was reverted successful.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
