<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_095111_create_table_region extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%region}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);

        $this->insert('{{%region}}', ['name' => 'Винницкая']);
        $this->insert('{{%region}}', ['name' => 'Волынская']);
        $this->insert('{{%region}}', ['name' => 'Днепропетровская']);
        $this->insert('{{%region}}', ['name' => 'Донецкая']);
        $this->insert('{{%region}}', ['name' => 'Житомирская']);
        $this->insert('{{%region}}', ['name' => 'Закарпатская']);
        $this->insert('{{%region}}', ['name' => 'Запорожская']);
        $this->insert('{{%region}}', ['name' => 'Ивано-Франковская']);
        $this->insert('{{%region}}', ['name' => 'Киевская']);
        $this->insert('{{%region}}', ['name' => 'Кировоградская']);
        $this->insert('{{%region}}', ['name' => 'Луганская']);
        $this->insert('{{%region}}', ['name' => 'Львовская']);
        $this->insert('{{%region}}', ['name' => 'Николаевская']);
        $this->insert('{{%region}}', ['name' => 'Одесская']);
        $this->insert('{{%region}}', ['name' => 'Полтавская']);
        $this->insert('{{%region}}', ['name' => 'Ровенская']);
        $this->insert('{{%region}}', ['name' => 'Сумская']);
        $this->insert('{{%region}}', ['name' => 'Тернопольская']);
        $this->insert('{{%region}}', ['name' => 'Харьковская']);
        $this->insert('{{%region}}', ['name' => 'Херсонская']);
        $this->insert('{{%region}}', ['name' => 'Черкасская']);
        $this->insert('{{%region}}', ['name' => 'Черниговская']);
        $this->insert('{{%region}}', ['name' => 'Черновицкая']);
    }

    public function safeDown()
    {
        echo "m160205_095111_create_table_region cannot be reverted.\n";

        $this->dropTable('{{%region}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
