<?php

use yii\db\Schema;
use yii\db\Migration;

class m160206_131552_create_table_clubs extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%clubs}}',[
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(255)->notNull(),
            'region_id' => $this->string(255)->notNull(),
            'latitude' => $this->integer()->defaultValue(1),
            'longitude' => $this->integer()->defaultValue(1),
            'photo' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%clubs}}');
        echo "m160206_131552_create_table_clubs was reverted successful.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
