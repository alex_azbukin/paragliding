<?php

use yii\db\Schema;
use yii\db\Migration;

class m160213_120244_add_column_upproved extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'upproved', 'SMALLINT(8)', 'DEFAULT VALUE(0) AFTER `registration_token`');
    }

    public function safeDown()
    {
        echo "m160213_120244_add_column_upproved cannot be reverted.\n";

        $this->dropColumn('{{%user}}', 'upproved', 'SMALLINT(8)', 'DEFAULT(0)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
