<?php

use yii\db\Schema;
use yii\db\Migration;

class m160213_115723_add_column_registration_token extends Migration
{
    public function safeup()
    {
        $this->addColumn('{{%user}}', 'registration_token', 'VARCHAR(255) AFTER `password_reset_token`');
    }

    public function safeDown()
    {
        echo "m160213_115723_add_column_registration_token cannot be reverted.\n";

        $this->dropColumn('{{%user}}', 'registration_token', 'VARCHAR(255)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
