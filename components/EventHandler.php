<?php
namespace app\components;

use yii\base\Component;
use yii\base\Event;

class EventHandler extends Component
{
    const EVENT_USER_REGISTERED = 'user_registered';
    const EVENT_RESET_PASSWORD = 'reset_password';
    const EVENT_CONFIRMATION = 'confirmation';
    const EVENT_CLAIM_PILOT = 'claim_pilot';
    const EVENT_REJECT_CLAIM_PILOT = 'reject_claim_pilot';
    const EVENT_APPROVE_CLAIM_PILOT = 'approve_claim_pilot';

    public function userRegistered(){
        $this->trigger(self::EVENT_USER_REGISTERED);
    }

    public function resetPassword(){
        $this->trigger(self::EVENT_RESET_PASSWORD);
    }

    public function confirmation(){
        $this->trigger(self::EVENT_CONFIRMATION);
    }

    public function claimPilot(){
        $this->trigger(self::EVENT_CLAIM_PILOT);
    }

    public function rejectClaimPilot(){
        $this->trigger(self::EVENT_REJECT_CLAIM_PILOT);
    }

    public function approveClaimPilot(){
        $this->trigger(self::EVENT_APPROVE_CLAIM_PILOT);
    }
}
