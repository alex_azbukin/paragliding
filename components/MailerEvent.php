<?php
namespace app\components;

use app\models\CustomMailer;
use app\models\User;
use yii\base\Component;
use yii\base\Event;

class MailerEvent extends Event
{
    public $user;

    public function userRegistered(){
        $admins = User::find()->where(['role_id' => 4])->all();
        CustomMailer::userRegisteredEvent($admins, null, ['user' => $this->user]);
    }

    public function resetPassword(){
        CustomMailer::resetPasswordEvent($this->user->email, null, ['user' => $this->user]);
    }

    public function confirmation(){
        CustomMailer::confirmationEvent($this->user->email, null, ['user' => $this->user]);
    }

    public function claimPilot(){
        $admins = User::find()->where(['role_id' => 4])->all();
        CustomMailer::claimPilot($admins, null, ['user' => $this->user]);
    }

    public function rejectClaimPilot(){
        CustomMailer::claimPilotAnswer($this->user->email, null, ['status' => 'rejected']);
    }

    public function approveClaimPilot(){
        CustomMailer::claimPilotAnswer($this->user->email, null, ['status' => 'approved']);
    }

}