<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="navbar-admin">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <ul class="nav navbar-nav">
                    <li>
                        <?= Html::a(Yii::t('app', 'Users'), Url::toRoute(['/admin/board/users']), ['class' => ' '.$active_link_users]);?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Clubs'), Url::toRoute(['/admin/board/clubs']), ['class' => ' '.$active_link_clubs]);?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Products'), Url::toRoute(['/admin/board/products']), ['class' => ' '.$active_link_products]);?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Suspension'), Url::toRoute(['/admin/board/suspensions']), ['class' => ' '.$active_link_suspensions]);?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('app', 'Equipment'), Url::toRoute(['/admin/board/equipments']), ['class' => ' '.$active_link_equipments]);?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>