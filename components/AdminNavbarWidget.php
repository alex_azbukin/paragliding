<?php
namespace app\components;

use yii\base\Widget;

class AdminNavbarWidget extends Widget
{
    public $active_link;
    public $active_link_users;
    public $active_link_clubs;
    public $active_link_products;
    public $active_link_suspensions;
    public $active_link_equipments;

    public function init()
    {
        parent::init();
        $this->active_link_users = '';
        $this->active_link_clubs = '';
        $this->active_link_products = '';
        $this->active_link_suspensions = '';
        $this->active_link_equipments = '';

        switch($this->active_link){
            case 'users':
                $this->active_link_users = 'active';
                break;
            case 'clubs':
                $this->active_link_clubs = 'active';
                break;
            case 'products':
                $this->active_link_products = 'active';
                break;
            case 'suspensions':
                $this->active_link_suspensions = 'active';
                break;
            case 'equipments':
                $this->active_link_equipments = 'active';
                break;
        }
    }

    public function run()
    {
        return $this->render('admin-navbar-widget', [
            'active_link_users' => $this->active_link_users,
            'active_link_clubs' => $this->active_link_clubs,
            'active_link_products' => $this->active_link_products,
            'active_link_suspensions' => $this->active_link_suspensions,
            'active_link_equipments' => $this->active_link_equipments,
        ]);
    }
}